//
//  Agenda_TableViewCell.m
//  Event Buoy 2.0 Phase-1
//
//  Created by Alet Viegas on 05/06/14.
//  Copyright (c) 2014 Shailesh. All rights reserved.
//

#import "Agenda_TableViewCell.h"
#import "Agenda_ViewController.h"

extern NSArray *agendaRecordReloadArray;
extern BOOL statusOfEvent;
extern int rowSelected;
extern NSArray *eventStatusArray;
NSString *getFilePath();

@implementation Agenda_TableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}





- (IBAction)scheduleTrackBtnAction:(UIButton *)sender
{
    rowSelected=sender.tag;
    statusOfEvent = 0;
    
    NSLog(@"%@",agendaRecordReloadArray);
    NSDictionary *agendaRecodsDict = [agendaRecordReloadArray objectAtIndex:rowSelected];   //  getting Entries in the Dictionary
    NSLog(@"%@",agendaRecodsDict);
    int selectedID= [[agendaRecodsDict objectForKey:@"ID"]integerValue];   // getting selected ID,
    
    for (NSDictionary * eventDict in eventStatusArray) // Search for Dictionary in an array
    {
        int ID = [[eventDict valueForKey:@"ID"]integerValue];    //
        
        if (ID==selectedID)
        {
            statusOfEvent = [[eventDict valueForKey:@"STATUS"]integerValue];
            
            break;
        }
    }
    
    NSString *alertTitle;
    NSString *alertMessage;    // Message To show in alet View
    if (statusOfEvent==1)
    {
        alertTitle=@"Remove From Schedule";
        alertMessage=@" Do You Want To Remove This Event From Schedule ? ";
    }
    else
    {
        alertTitle=@"Add to Schedule";
        alertMessage=@" Do You Want To Add This Event To Schedule ? ";
    }
    
    UIAlertView *scheduleAlert = [[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
    [scheduleAlert show];
    
    NSLog(@"%@",agendaRecodsDict);
    
}


#pragma mark - UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex  //1. fetch selected ID   2. Update it in Database  3. update it in Array
{
    if (buttonIndex == 0)
    {
        NSMutableArray *tempArray = [[NSMutableArray alloc]initWithArray:agendaRecordReloadArray];
        NSNumber * selectedEventID = [[tempArray objectAtIndex:rowSelected]objectForKey:@"ID"];
        NSLog(@"%@",selectedEventID);
        
        BOOL scheduledAddedFlag =0; // Flag is use to check that event is already found in an array
        
        NSMutableArray *eventStatusTemp = [[NSMutableArray alloc]initWithArray:eventStatusArray];
        
        //   NSMutableDictionary *data = [[NSMutableDictionary alloc]initWithDictionary:[tempArray objectAtIndex:rowSelected]];
        
        NSMutableDictionary *data;
        
        int indexOfEvent=0; // index of dict found in an array
        
        for (NSDictionary * eventDict in eventStatusArray) // Search for Dictionary in an array
        {
            int ID = [[eventDict valueForKey:@"ID"]integerValue];    //
            
            if (ID == [selectedEventID integerValue])
            {
                statusOfEvent = [[eventDict valueForKey:@"STATUS"]integerValue];
                
                data = [[NSMutableDictionary alloc]initWithDictionary:eventDict];
                
                scheduledAddedFlag = 1;
                
                break;
            }
            
            indexOfEvent++;
        }
        
        
        if (statusOfEvent == 1)   // Here We going to remove event from scheeduled events
        {
            [self updateDatabaseStatus:selectedEventID:@"0"];   // update status in EVENT_STATUS Table
            [data setObject:[NSNumber numberWithInt:0] forKey:@"STATUS"];  // update in Dictionary
            [eventStatusTemp replaceObjectAtIndex:indexOfEvent withObject:data];
        }
        else   // Here We going to Add event in scheeduled events
        {
            [self updateDatabaseStatus:selectedEventID:@"1"];
            data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:selectedEventID,@"ID",@"1",@"STATUS", nil];
            NSLog(@"%@",data);
            if (scheduledAddedFlag)
                [eventStatusTemp replaceObjectAtIndex:indexOfEvent withObject:data];
            else
                [eventStatusTemp addObject:data];
        }
        
        eventStatusArray = eventStatusTemp;
        
        NSLog(@"%@",eventStatusArray);
        
        Agenda_ViewController *objAgendaVC=[[Agenda_ViewController alloc]init];
        [objAgendaVC.agendaTableView reloadData];
        
    }
    
}


#pragma mark - Update The Status Of Selected Event as 1.

- (void)updateDatabaseStatus : (NSNumber*)selectedEventID : (NSString*)replaceString
{
    NSString *databasePath=getFilePath();
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    sqlite3 *database;
    sqlite3_stmt *statement;
    
    if ([filemgr fileExistsAtPath: databasePath])
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            NSString *insertSQL=[[NSString alloc]init];
            
            //insertSQL = [NSString stringWithFormat: @"REPLACE INTO EVENT(ID,USERNAME,DRINKNAME,AMOUNT,QUANTITY,PACK) VALUES ('%d','%@','%@','%d','%d','%@')",duplicateRecordID, userName,drinkName,drinkAmount,quantityToUpdate+1,packSize];
            
            insertSQL = [NSString stringWithFormat:@"REPLACE INTO EVENT_STATUS (ID,STATUS) VALUES ('%@','%@')",selectedEventID,replaceString];
            
            const char *insert_stmt = [insertSQL UTF8String];
            
            sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL);
            
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                
            }
            else
                NSLog(@"error");
            
            sqlite3_finalize(statement);
            
            sqlite3_close(database);
            
        }
    }
    
}



- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
