//
//  EventDetailViewController.m
//  Nasscom_Application 1.0
//
//  Created by Black Bean Engagement on 11/02/14.
//  Copyright (c) 2014 Black Bean Engagement. All rights reserved.
//

#import "ViewController.h"
#import "EventDetailViewController.h"
#import "Agenda_ViewController.h"
#import "InfoCustomCell.h"

#import <sqlite3.h>

@interface EventDetailViewController ()
{
    InfoCustomCell *objOfInfoCustomCell;
}

@property (nonatomic,retain) IBOutlet UITableView *tableView,*tbl2;  // table View To show Info,etc.
@property (nonatomic,retain) IBOutlet NSArray *titleArray; // Array to Store Name of Roe Title
@property (nonatomic,retain) IBOutlet NSArray *infoArray;
@property (nonatomic,retain) IBOutlet UILabel *eventName; // event Name Label
@property (nonatomic,retain) IBOutlet UILabel *eventDate; // event Date Label
@property (nonatomic,retain) IBOutlet UIButton *sidebarButton; // Button To go to Main Menu
@property (nonatomic,retain) IBOutlet UILabel *eventTime; // Label to show time of an event.
@property (weak, nonatomic) IBOutlet UILabel *eventDetailTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *showMenuButton;

- (NSNumber*)getSelectedEventID;  // Method To get Selected Event ID Written in .plist file
- (NSDictionary*)getEventInfo:(NSNumber*)selectedID;   // Method To get Event Info about selected Event ID
- (void)getNoteOfEvent; // Method To get Note About Event

@end


int strDescriptionLenght;
int selectedIndex = -1;
int countOfSpeakers = 0;

NSString *eventDetailTitleTextColor;
NSString *detailRowBackgroundColor;
NSString *detailRowtextColor;
NSString *menuButtonImageName;
NSData *rowClosedImgData;
NSData *rowOpenImgData;

extern NSString *getFilePath();    // Method To get file Path of Database
NSArray *numberofSpeakersArray; // Array to get Total numbers of Speakers for an event.
NSDictionary *eventRecordDictionary;
int selectedEventID;
NSMutableDictionary *noteInfoDict; // Written Info About the Event

extern NSString *getMenuItemImagesPath();

@implementation EventDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
       // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad   // 1.Read the selected Event ID from .plist   2. show related Info about the Event
{
    [super viewDidLoad];
    
    
    eventDetailTitleTextColor=@"2981B3";
    menuButtonImageName=@"eventlogo.png";
    detailRowBackgroundColor = @"E37A70";
    detailRowtextColor=@"000000";
    
    objOfInfoCustomCell=[[InfoCustomCell alloc]init];
    
    _titleArray = [[NSArray alloc]initWithObjects:@"INFO",@"SPEAKERS",@"NOTE", nil];
    
    NSNumber *selectedID = [self getSelectedEventID];
    eventRecordDictionary = [self getEventInfo:selectedID];
    
    _infoArray = [[NSArray alloc]initWithObjects:[eventRecordDictionary objectForKey:@"LOCATION"],[eventRecordDictionary objectForKey:@"CATEGORY"],[eventRecordDictionary objectForKey:@"DESCRIPTION"], nil];
    
    NSString *strDescription=[_infoArray objectAtIndex:2]; // Taking description letters lenght for loading the cell width size of INFO cell.
    strDescriptionLenght=[strDescription length];
    NSLog(@"len=%d",strDescriptionLenght);
    
    NSLog(@"infoArray :%@",_infoArray);
    
    NSLog(@"EventRecodsDict :%@",eventRecordDictionary);
    if ([[eventRecordDictionary objectForKey:@"SPEAKERS_ID"] isEqualToString:@""])
        numberofSpeakersArray =Nil;
    else
        numberofSpeakersArray = [[eventRecordDictionary objectForKey:@"SPEAKERS_ID"] componentsSeparatedByString:@","];
    
    countOfSpeakers = [numberofSpeakersArray count];
    NSNumber *IDnumber = [eventRecordDictionary objectForKey:@"ID"];
    selectedEventID = [IDnumber intValue];
    
    _eventDetailTitleLabel.textColor=[HexColorConvert colorWithHexString:eventDetailTitleTextColor];
    _eventName.textColor = [HexColorConvert colorWithHexString:eventDetailTitleTextColor];
    _eventDate.textColor = [HexColorConvert colorWithHexString:eventDetailTitleTextColor];
    _eventTime.textColor = [HexColorConvert colorWithHexString:eventDetailTitleTextColor];
    
    NSString *agendaScreeImageFolderPath = getEventDetailImagesPath(nil);
    
    NSString *filePathForBackBtnImage = [agendaScreeImageFolderPath stringByAppendingPathComponent:@"Row_Closed.png"];
    rowClosedImgData = [NSData dataWithContentsOfFile:filePathForBackBtnImage];
    
    NSString *filePathForNextBtnImage = [agendaScreeImageFolderPath stringByAppendingPathComponent:@"Row_Open.png"];
    rowOpenImgData = [NSData dataWithContentsOfFile:filePathForNextBtnImage];
    
    
    NSString *evenMenuScreenImagesPath = getMenuItemImagesPath(nil);
    
    NSString* filePathForEvetLogoButton = [evenMenuScreenImagesPath stringByAppendingPathComponent:[NSString stringWithString:menuButtonImageName]];
    NSData* eventLogoImageData = [NSData dataWithContentsOfFile:filePathForEvetLogoButton];
    
    [_showMenuButton setBackgroundImage:[UIImage imageWithData:eventLogoImageData] forState:UIControlStateNormal];
    
    NSLog(@"%d",selectedEventID);
    NSLog(@"%@",numberofSpeakersArray);
    NSLog(@"%@",eventRecordDictionary);
    
    _eventName.text = [eventRecordDictionary objectForKey:@"NAME"];
    _eventTime.text = [eventRecordDictionary objectForKey:@"TIME"];
    _eventDate.text = [NSString stringWithFormat:@"%@, %@",[eventRecordDictionary objectForKey:@"DAY"],[eventRecordDictionary objectForKey:@"DATE"]];
    
    //_tableView.scrollEnabled=NO;
    
    //[self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithRed:41.0/255.0 green:41.0/255.0 blue:41.0/255.0 alpha:1]];
    
    
    
    
	


}- (void)viewWillAppear:(BOOL)animated
{
    noteInfoDict =Nil;
    [self getNoteOfEvent];
    NSString *note = [noteInfoDict objectForKey:@"NOTE"];
    NSInteger stringLength = [note length];
    NSLog(@"%d",stringLength);
    
  //  [_sidebarButton addTarget:self.ViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
  //  [self.view addGestureRecognizer:self.ViewController.panGestureRecognizer];
    
    [_tableView reloadData];    
    
}
#pragma mark - Method To get Selected Event ID Written in .plist file

- (NSNumber*)getSelectedEventID
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"Tiecon.plist"];
    
    NSMutableDictionary *data;
    
    data = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    
    NSNumber *SelectedID = [data objectForKey:@"SelectedEvent"];
    //To insert the data into the plist
    
    return SelectedID;

}

#pragma mark - Method To get Event Info about selected Event ID

- (NSDictionary*)getEventInfo:(NSNumber*)selectedID
{
    NSDictionary *recordDictionary;
    
    NSString *databasePath=getFilePath();
    
    sqlite3 *database;
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: databasePath])
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            NSString *sqlStatement = [NSString stringWithFormat:@"SELECT * FROM EVENT WHERE ID = '%@'",selectedID];
            
            sqlite3_stmt *statement;
            
            if(sqlite3_prepare_v2(database, [sqlStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
            {
                while(sqlite3_step(statement) == SQLITE_ROW)
                {
                    NSInteger ID=sqlite3_column_int(statement, 0);
                    NSString *NAME=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,1)];
                    NSString *DAY=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,2)];
                    NSString *DATE=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,3)];
                    NSString *TIME=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,4)];
                    NSString *LOCATION=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,5)];
                    NSString *CATEGORY=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,6)];
                    NSString *SPEAKERS_ID=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,7)];
                    NSString *DESCRIPTION=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,8)];
                    
                    
                    recordDictionary=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:ID],@"ID",NAME,@"NAME",DAY,@"DAY",DATE,@"DATE",TIME,@"TIME",LOCATION,@"LOCATION",CATEGORY,@"CATEGORY",SPEAKERS_ID,@"SPEAKERS_ID",DESCRIPTION,@"DESCRIPTION",Nil];      // getting record on dictionary
                    
                }
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    
    return recordDictionary;
    
}


- (void)getNoteOfEvent
{
    NSString *databasePath=getFilePath();
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    sqlite3 *database;
    
    if ([filemgr fileExistsAtPath: databasePath])
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            NSString * sqlStatement = [NSString stringWithFormat:@"SELECT * FROM NOTE WHERE ID = '%@'",[NSNumber numberWithInt:selectedEventID]];
            
            sqlite3_stmt *statement;
            
            if(sqlite3_prepare_v2(database, [sqlStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
            {
                while(sqlite3_step(statement) == SQLITE_ROW)
                {
                    NSInteger ID=sqlite3_column_int(statement, 0);
                    
                    NSString *NOTE=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,1)];
                    
                    noteInfoDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:ID],@"ID",NOTE,@"NOTE",Nil];      // getting record on dictionary
                }
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    NSLog(@"%@",noteInfoDict);
}




- (void)viewWillDisappear:(BOOL)animated
{
    selectedIndex = -1;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==1)
        return _titleArray.count;
    else
        return countOfSpeakers;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier =@"InfoCustomCell";
    
    InfoCustomCell *cell = (InfoCustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == Nil)
    {
        cell = [[InfoCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }

//*
    cell.eventInfoTextView.hidden=YES;
    cell.speakersTableView.hidden=YES;
    cell.noteView.hidden = YES;
    
        if (selectedIndex == indexPath.row) // Exapnsion
    {
        cell.rowArrowImage.image = [UIImage imageWithData:rowOpenImgData];
        
        if (indexPath.row == 0)   //  Show Info
        {
//            cell.speakersTableView.hidden=YES;
//            cell.noteView.hidden=YES;
           
             cell.eventInfoTextView.hidden= NO;
            cell.eventInfoTextView.textColor=[HexColorConvert colorWithHexString:eventDetailTitleTextColor];
           
        }
        else if (indexPath.row == 1)  // Show List of Speakers
        {
            if (countOfSpeakers<4)
            {
                cell.speakersTableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 41, 280,  countOfSpeakers * 44)];
            }
            else
            {
                cell.speakersTableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 41, 280,  countOfSpeakers * 44)];
            }
            cell.speakersTableView.delegate=cell;
            cell.speakersTableView.dataSource=cell;
            cell.speakersTableView.separatorColor=[UIColor redColor];
            [cell.contentView addSubview:cell.speakersTableView];
            cell.speakersTableView.hidden=YES;
          //  [cell.speakersTableView reloadData];

            cell.speakersTableView.hidden = NO;
           // [cell.contentView setNeedsDisplay];
//            dispatch_async(kBgQueue, ^{
//                if (countOfSpeakers<4)
//                {
//                    cell.speakersTableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 41, 280, 42 + countOfSpeakers * 44)];
//                }
//                else
//                {
//                    cell.speakersTableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 41, 280, 42 + countOfSpeakers * 44)];
//                }
//                 cell.speakersTableView.separatorColor=[UIColor redColor];
//                
//                dispatch_async(dispatch_get_main_queue(), ^(void)
//                               {
//                                   [cell.contentView addSubview:cell.speakersTableView];
//                                   [cell.speakersTableView setNeedsDisplay];
//                                   cell.speakersTableView.hidden = NO;
//                               });
//                        });

           // [cell.contentView addSubview:cell.speakersTableView];
          //  [cell.speakersTableView setNeedsDisplay];
            
           // [cell.contentView addSubview:cell.speakersTableView];
            
          //  [cell setNeedsDisplay];
          //  [cell.speakersTableView reloadInputViews];
            
            
        }
        
        else if (indexPath.row == 2)   // Show Note View
        {
            
            cell.noteView.hidden = NO;
          //  cell.speakersTableView.hidden=YES;
            cell.noteTextView.text = [noteInfoDict objectForKey:@"NOTE"];
            cell.noteTextView.textColor = [HexColorConvert colorWithHexString:eventDetailTitleTextColor];
            cell.noteTextView.backgroundColor = [HexColorConvert colorWithHexString:detailRowBackgroundColor];
        }
    }
    else  // Closed
    {
        cell.rowArrowImage.image = [UIImage imageWithData:rowClosedImgData];
    }
    
    cell.infoTitleLabel.text = [_titleArray objectAtIndex:indexPath.row];
    cell.infoTitleLabel.textColor = [HexColorConvert colorWithHexString:detailRowtextColor];
    cell.cellImageBackground.backgroundColor = [HexColorConvert colorWithHexString:detailRowBackgroundColor];
    cell.locationTitleLabel.text= [_infoArray objectAtIndex:0];
    cell.categoryTitleLabel.text= [_infoArray objectAtIndex:1];
    cell.decriptionTitleLabel.text= [_infoArray objectAtIndex:2];
    
    
    
    cell.eventInfoTextView.text = [NSString stringWithFormat:@"Location : %@ \nCategory : %@ \nDescription : %@",[_infoArray objectAtIndex:0],[_infoArray objectAtIndex:1],[_infoArray objectAtIndex:2]];
// */
    
 
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectedIndex == indexPath.row)
    {
        switch (indexPath.row)
        {
            case 0:
                 if (strDescriptionLenght<150)
                {
                    return 117;
                }
                else if (strDescriptionLenght<235)
                {
                    return 158;
                }
                return 208;
                break;
                
            case 1:
                if (countOfSpeakers<4)
                {
                    return 42 + countOfSpeakers * 44;
                }
                return 42 + countOfSpeakers * 44;

                  break;
                
            case 2: return 84 + 120;
                break;
                
            default:
                break;
        }
        
    }
    
    return 42;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectedIndex == indexPath.row)
    {
        selectedIndex=-1;
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        return;
    }
    
    if (selectedIndex != -1)
    {
        NSIndexPath *prevPath = [NSIndexPath indexPathForRow:selectedIndex inSection:0];
        selectedIndex = indexPath.row;
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:prevPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    selectedIndex = indexPath.row;
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
    
}

NSString *getEventDetailImagesPath()
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *sponsorsFolder = [documentsPath stringByAppendingPathComponent:@"/EventDetailsImages/"];
    
    return sponsorsFolder;
}


@end
