//
//  SpeakersViewController.m
//  Nasscom_Application 1.0
//
//  Created by Black Bean Engagement on 08/02/14.
//  Copyright (c) 2014 Black Bean Engagement. All rights reserved.
//

#import "SpeakersViewController.h"
#import "ViewController.h"
#import "SpeakersCustomCell.h"
#import "ListOfSpeakersCell.h"
#import "Reachability.h"
#import <sqlite3.h>
#import "AppDelegate.h"
#import "AsyncImageView.h"
#import "HexColorConvert.h"

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1

@interface SpeakersViewController ()

@property (weak, nonatomic) IBOutlet UIButton *sidebarButton;    // Button on Navigation Bar.
@property (weak, nonatomic) IBOutlet UITableView *listOfSpeakersTableView;  // Table to show list of Speakers.
@property (retain, nonatomic) NSArray *listOfSpeakers; // Array To get all Speakers from Database.
@property (retain,nonatomic)  UIActivityIndicatorView *activityIndicator; // activity indicator to show loading


- (NSArray *)getListOfSpeakers; // Method to get List Of Speakers in an Array.
- (void)writeToFile :(NSNumber*)selectedID ; // Write Slelected Speaker ID to .plist file
- (void)AddEntries : (NSArray*)speakerDataArray; // Method To write Fetched Data To Database
//-(void)loadImagesToFiles; // Method To Speakers Logo All Images in an File.
//-(NSString*)getSpeakerImagesFilePath; // Method To get Speakers Logo Folder Path
- (void)speakersOperation; // Method to work on speakers in background queue.

NSString *getSpeakerImagesFilePath(NSString*);


@end

@implementation SpeakersViewController

extern NSString *getFilePath();    // Method To get file Path of Database
extern int speakerFlag; // Flag to access speakers data only once

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad   // 1.Load all the speakers from Databasein an array  2.Load Custom Cell  3.Show Data From an Array to custom cell
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
   // [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"action-bar320-65.png"] forBarMetrics:UIBarMetricsDefault];
  //  self.navigationController.navigationBar.barTintColor = [UIColor redColor];
    
    _activityIndicator.hidesWhenStopped=YES;
    
    _listOfSpeakers = [self getListOfSpeakers];
    
    // [self loadImagesToFiles];  // add Images to folder

#pragma mark - Checking use of speakers first time or second time.
    
    id useofSpeaker = [[NSUserDefaults standardUserDefaults] objectForKey:@"UseOfSpeakers"];
    NSInteger intValueuseofSpeakers=[useofSpeaker integerValue];
    useofSpeakers=[NSNumber numberWithInt:intValueuseofSpeakers];
    check=useofSpeakers;
    if (![useofSpeakers isEqualToNumber:[NSNumber numberWithInt:1]])
    {
       
        NSNumber *jsonSpeakers=[NSNumber numberWithInt:1];
        [[NSUserDefaults standardUserDefaults] setObject:jsonSpeakers forKey:@"UseOfSpeakers"];
    }
    id useofSpeaker2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"UseOfSpeakers"];
    NSInteger intValueuseofSpeakers2=[useofSpeaker2 integerValue];
    useofSpeakers=[NSNumber numberWithInt:intValueuseofSpeakers2];

    NSLog(@"useofSpeakers : %@",useofSpeakers);
    
    id jsonSpeakers = [[NSUserDefaults standardUserDefaults] objectForKey:@"JsoncountSpeakers"];
    NSInteger intVlaueSeakers=[jsonSpeakers integerValue];
    jsonCountSpeakers=[NSNumber numberWithInt:intVlaueSeakers];

  
    
    if (![jsonCountSpeakers isEqualToNumber:[NSNumber numberWithInt:[_listOfSpeakers count]]] || [_listOfSpeakers count]==0 )   // if no data found or less data found in database
    {
        Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
        
        if (networkStatus == NotReachable)
        {
            UIAlertView *networkAlert= [[UIAlertView alloc]initWithTitle:@"Network Alert" message:@"Please Connect to Internet To access this Service " delegate:Nil cancelButtonTitle:@"ok" otherButtonTitles:Nil, nil];
            [networkAlert show];
        }
        else
        {
            [_activityIndicator startAnimating];
            if (speakerFlag== 0)
            {
                speakerFlag=1;
                NSURL *url=[NSURL URLWithString:@"http://listoftablets.com/tiecon/admin/getSpeakers.php"];
                dispatch_async(kBgQueue, ^{NSData* data = [NSData dataWithContentsOfURL : url];
                    
                    NSDictionary* json = [NSJSONSerialization
                                          JSONObjectWithData:data // 1
                                          
                                          options:kNilOptions
                                          error:Nil];
                    
                    NSArray* speakers = [json objectForKey:@"speakers_detais"]; //2
                    
                    NSNumber *jsonSpeakers=[NSNumber numberWithInt:[speakers count]];
                    [[NSUserDefaults standardUserDefaults] setObject:jsonSpeakers forKey:@"JsoncountSpeakers"];
                    [self AddEntries:speakers];                
                    _listOfSpeakers = [self getListOfSpeakers]; // getting Records in an Array from Database
                    [_listOfSpeakersTableView reloadData];
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void)
                    {    // 1.Add Fetched Data To Database   2.Load it in an Array  3.Show in a Table View
                        
                        [_listOfSpeakersTableView reloadData];
                        
                        [_activityIndicator stopAnimating];

                    });
                    
                   [self loadImagesToFiles];
                    
//                    dispatch_async(dispatch_get_main_queue(), ^(void)
//                                   {    // 1.Add Fetched Data To Database   2.Load it in an Array  3.Show in a Table View
//                                       
////                                       [_listOfSpeakersTableView reloadData];
////                                       
////                                       
////                                       [_activityIndicator stopAnimating];
//                                   });
                
                });
                
            }
        }
    }
    else
        [self getListOfSpeakers];
    
    NSLog(@"%@",_listOfSpeakers);
    
}


- (void)viewWillAppear:(BOOL)animated
{
   // [_sidebarButton addTarget:self.ViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
 
    self.navigationController.navigationBar.barTintColor = [HexColorConvert colorWithHexString:@"1FAD90"];  //Set color of navigation bar with help of HexClorConverter.
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];                               //Navigation back button color set to white.
    // Set the gesture
   // [self.view addGestureRecognizer:self.ViewController.panGestureRecognizer];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Method to work on speakers in background queue.

- (void)speakersOperation
{
    


}

#pragma mark - Table View Datasource Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_listOfSpeakers count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier =@"ListOfSpeakersCustomCell";
    
    ListOfSpeakersCell *cell = (ListOfSpeakersCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    #define IMAGE_VIEW_TAG 99
    
//    if (cell == Nil)
//    {
//        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ListOfSpeakersCustomCell" owner:self options:nil];
//        cell = [nibArray objectAtIndex:0];
    
        AsyncImageView *imageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(20, 5, 42, 42)];
		imageView.contentMode = UIViewContentModeScaleAspectFill;
		imageView.clipsToBounds = YES;
		imageView.tag = IMAGE_VIEW_TAG;
		[cell addSubview:imageView];
        
        
  //  }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    cell.speakerName.text = [[_listOfSpeakers objectAtIndex:indexPath.row] valueForKey:@"NAME"];
    cell.speakerDesignation.text = [[_listOfSpeakers objectAtIndex:indexPath.row] valueForKey:@"DETAILS1"];
    
    
    NSLog(@"useofspeaker : %@",useofSpeakers);
    
    
    if (![jsonCountSpeakers isEqualToNumber:[NSNumber numberWithInt:[_listOfSpeakers count]]] || [_listOfSpeakers count]==0 || [check isEqualToNumber:[NSNumber numberWithInt:0]])   // if present in database goto else
    {

        //  AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:IMAGE_VIEW_TAG];
        //[cell.speakerImage setHidden:YES];
        imageView.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://listoftablets.com/tiecon/uploads/%@",[[_listOfSpeakers objectAtIndex:indexPath.row] valueForKey:@"IMAGE"]]];
    
      //  NSLog(@"%d",indexPath.row);
    }
    else
    {
      /* First one before GCD
        [cell.speakerImage setHidden:NO];
        NSString *sponsorsFilePath = getSpeakerImagesFilePath(nil) ;
        NSString *filePath = [sponsorsFilePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[[_listOfSpeakers objectAtIndex:indexPath.row] valueForKey:@"IMAGE"]]];
        NSData *pngData = [NSData dataWithContentsOfFile:filePath];
        UIImage *image = [UIImage imageWithData:pngData];
        cell.speakerImage.image = image;
     */
        //Secodeone use of GCD
        [cell.speakerImage setHidden:NO];

        dispatch_async(kBgQueue, ^{
            NSString *sponsorsFilePath = getSpeakerImagesFilePath(nil) ;
            NSString *filePath = [sponsorsFilePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[[_listOfSpeakers objectAtIndex:indexPath.row] valueForKey:@"IMAGE"]]];
            NSData *pngData = [NSData dataWithContentsOfFile:filePath];
            UIImage *image = [UIImage imageWithData:pngData];
            
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                               cell.speakerImage.image = image;
                               
                           });
                        });
        
    }
    
  

    
    
    
    
   
//    if ([[_listOfSpeakers objectAtIndex:indexPath.row] valueForKey:@"IMAGE"])
//    {
//        
//        [cell.speakerImage setImage:[UIImage imageWithData:pngData]];
//    }
//    else
//    {
//    
//        dispatch_async(kBgQueue, ^{
//       
//        [cell.speakerImage setImage:[UIImage imageWithData:pngData]];
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//            [_listOfSpeakersTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
//        });
//                             
//        });
//    
//    }
    
    return cell;
    
}

#pragma mark - Table View Delegate Methods

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber *IDnumber = [[_listOfSpeakers objectAtIndex:indexPath.row] valueForKey:@"ID"];
    NSLog(@"%@",_listOfSpeakers);
    NSLog(@"%@",IDnumber);
    [self writeToFile:IDnumber];
}


#pragma mark - Method to get List Of Speakers in an Array.

- (NSArray *)getListOfSpeakers
{
    
    NSMutableArray *speakerArray = [[NSMutableArray alloc]init];
    
    
    NSString *databasePath=getFilePath();
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    sqlite3 *database;
    
    if ([filemgr fileExistsAtPath: databasePath])
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            //const char *sqlStatement = "SELECT * FROM EVENT ";
            
            NSString *sqlStatement = [NSString stringWithFormat:@"SELECT * FROM SPEAKERS ORDER BY SPEAKER_ORDER"];
            
            sqlite3_stmt *statement;
            
            if(sqlite3_prepare_v2(database, [sqlStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
            {
                while(sqlite3_step(statement) == SQLITE_ROW)
                {
                    NSInteger ID=sqlite3_column_int(statement, 0);
                    NSString *NAME=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,1)];
                    NSString *DETAILS1=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,2)];
                    NSString *DETAILS2=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,3)];
                    NSString *IMAGE=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,4)];
                    NSString *INFO=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,5)];
                    NSString *SESSIONS=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,6)];
                    NSString *SOCIAL=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,7)];
                    
                    
                    NSDictionary *recordDictionary=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:ID],@"ID",NAME,@"NAME",DETAILS1,@"DETAILS1",DETAILS2,@"DETAILS2",IMAGE,@"IMAGE",INFO,@"INFO",SESSIONS,@"SESSIONS",SOCIAL,@"SOCIAL",Nil];      // getting record on dictionary
                    
                    [speakerArray addObject:recordDictionary];   // Adding Each Record in an Array
                }
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    return speakerArray;
}

#pragma mark - Write Slelected Speaker ID to .plist file

- (void)writeToFile :(NSNumber*)selectedID
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"Tiecon.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: path])
    {
        path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat: @"Tiecon.plist"] ];
    }
    
    NSMutableDictionary *data;
    
    if ([fileManager fileExistsAtPath: path])
    {
        data = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    }
    else
    {
        // If the file doesn’t exist, create an empty dictionary
        data = [[NSMutableDictionary alloc] init];
    }
    
    //To insert the data into the plist
    
    [data setObject:selectedID forKey:@"SelectedID"];
    
    [data writeToFile: path atomically:YES];
    
}


#pragma mark - Method To write Fetched Data To Database

- (void)AddEntries : (NSArray*)speakerDataArray
{
    NSString *databasePath=getFilePath();
    
    NSFileManager *filemgr = [NSFileManager defaultManager];

    sqlite3 *database;
    sqlite3_stmt *statement;
    
    if ([filemgr fileExistsAtPath: databasePath])
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            NSString *insertSQL=[[NSString alloc]init];
            
            for (int i=0; i < [speakerDataArray count]; i++)
            {
                NSString *details1 = [[speakerDataArray objectAtIndex:i] valueForKey:@"speaker_detail1"];
                NSLog(@"%@",details1);
                
                NSString *details2 = [[speakerDataArray objectAtIndex:i] valueForKey:@"speaker_detail2"];
                NSLog(@"%@",details2);
                
                NSString *url = [[speakerDataArray objectAtIndex:i] valueForKey:@"speaker_image_url"];
                NSLog(@"%@",url);
                
                NSString *info = [[speakerDataArray objectAtIndex:i] valueForKey:@"speaker_info"];
                NSLog(@"%@",info);
                info = [info stringByReplacingOccurrencesOfString:@"'" withString:@" "];
                info = [info stringByReplacingOccurrencesOfString:@"/" withString:@" "];
                
                NSLog(@"%@",info);
                
                NSString *sessions = [[speakerDataArray objectAtIndex:i] valueForKey:@"speaker_sessions"];
                NSLog(@"%@",details2);
                
                NSString *social = [[speakerDataArray objectAtIndex:i] valueForKey:@"social"] ;
                NSLog(@"%@",social);
                NSString *speakerOrder = [[speakerDataArray objectAtIndex:i] valueForKey:@"speaker_order"];
                
                insertSQL = [NSString stringWithFormat: @"REPLACE INTO SPEAKERS (ID ,NAME ,DETAILS1 , DETAILS2 , IMAGE , INFO ,SESSIONS ,SOCIAL ,SPEAKER_ORDER) VALUES ('%@','%@','%s','%s','%s','%s','%s','%s','%s')",[[speakerDataArray objectAtIndex:i] valueForKey:@"speaker_id"],[[speakerDataArray objectAtIndex:i] valueForKey:@"speaker_name"],[details1 UTF8String],[details2 UTF8String],[url UTF8String],[info UTF8String],[sessions UTF8String],[social UTF8String],[speakerOrder UTF8String]];
                
                const char *insert_stmt = [insertSQL UTF8String];
                char *errMsg = NULL;
                
                sqlite3_prepare_v2(database, insert_stmt, -1, &statement,Nil);
                
                if (sqlite3_exec(database, insert_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
                {
                
                }
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    
                }
                else
                    NSLog(@"error");
                
            }
            
            sqlite3_finalize(statement);
            
            sqlite3_close(database);
            
        }
    }
    
}


#pragma mark - Method To Load All Images in an File.

-(void)loadImagesToFiles
{
    NSString *sposnsorsFolderPath = getSpeakerImagesFilePath(nil);

    if (![[NSFileManager defaultManager] fileExistsAtPath:sposnsorsFolderPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:sposnsorsFolderPath withIntermediateDirectories:NO attributes:nil error:Nil]; //Create folder
    
    for (int i=0; i < [_listOfSpeakers count]; i++)
    {
        NSString *imageUrlString= [NSString stringWithFormat:@"http://listoftablets.com/tiecon/uploads/%@",[[_listOfSpeakers objectAtIndex:i] valueForKey:@"IMAGE"]];
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrlString]]];
        NSData *pngData = UIImagePNGRepresentation(image);
        
        NSString *filePath = [sposnsorsFolderPath stringByAppendingPathComponent:[[_listOfSpeakers objectAtIndex:i] valueForKey:@"IMAGE"]]; //Add the file name
        [pngData writeToFile:filePath atomically:YES]; //Write the file
    }
}

@end

#pragma mark - Method To get Sponsors Logo Folder Path

NSString *getSpeakerImagesFilePath()
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *sponsorsFolder = [documentsPath stringByAppendingPathComponent:@"/SpeakersImages/"];
    
    return sponsorsFolder;
}

