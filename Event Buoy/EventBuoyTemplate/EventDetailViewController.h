//
//  EventDetailViewController.h
//  Nasscom_Application 1.0
//
//  Created by Black Bean Engagement on 11/02/14.
//  Copyright (c) 2014 Black Bean Engagement. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDetailViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@end
