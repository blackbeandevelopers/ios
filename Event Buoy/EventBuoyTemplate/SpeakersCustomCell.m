//
//  SpeakersCustomCell.m
//  Nasscom_Application 1.0
//
//  Created by Black Bean Engagement on 15/02/14.
//  Copyright (c) 2014 Black Bean Engagement. All rights reserved.
//

#import "SpeakersCustomCell.h"

@implementation SpeakersCustomCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
