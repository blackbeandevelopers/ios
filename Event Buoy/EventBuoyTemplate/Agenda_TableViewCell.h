//
//  Agenda_TableViewCell.h
//  Event Buoy 2.0 Phase-1
//
//  Created by Alet Viegas on 05/06/14.
//  Copyright (c) 2014 Shailesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Agenda_TableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblAgendaTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAgendaVenue;
@property (weak, nonatomic) IBOutlet UILabel *lblAgendaTime;

@property (weak, nonatomic) IBOutlet UIButton *btnScheduleTrack;

@end
