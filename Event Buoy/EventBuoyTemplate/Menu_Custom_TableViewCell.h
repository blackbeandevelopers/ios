//
//  Menu_Custom_TableViewCell.h
//  Event Buoy 2.0 Phase-1
//
//  Created by Alet Viegas on 05/06/14.
//  Copyright (c) 2014 Shailesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Menu_Custom_TableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *menuItemImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblMenuItemName;
@end
