//
//  SpeakersCustomCell.h
//  Nasscom_Application 1.0
//
//  Created by Black Bean Engagement on 15/02/14.
//  Copyright (c) 2014 Black Bean Engagement. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpeakersCustomCell : UITableViewCell

@property (nonatomic,retain) IBOutlet UILabel *speakerName;
@property (nonatomic,retain) IBOutlet UILabel *speakerDesignation;
@property (nonatomic,retain) IBOutlet UIImageView *speakerImage;

@end
