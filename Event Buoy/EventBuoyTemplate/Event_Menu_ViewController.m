//
//  Event_Menu_ViewController.m
//  Event Buoy 2.0 Phase-1
//
//  Created by Alet Viegas on 05/06/14.
//  Copyright (c) 2014 Shailesh. All rights reserved.
//

#import "Event_Menu_ViewController.h"
#import "Menu_Custom_TableViewCell.h"


@interface Event_Menu_ViewController ()
@property (weak, nonatomic) IBOutlet UITableView *table;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewMainMenuLogo;

@end

NSString *tableBackgroundColor,*tableSeperatorColor;
NSString *menuItemsNameColor;
NSString *eventNavigationLogo;
NSMutableArray *menuItemImages,*menuItemNames;

NSString *mainHomeScreenArrowImgName;

@implementation Event_Menu_ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    menuItemsNameColor=@"2981B3";
    tableBackgroundColor=@"FFFFFF";
    tableSeperatorColor=@"2981B3";
    
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationItem.title=@"Menu";

    eventNavigationLogo=@"eventlogo.png";
    
    mainHomeScreenArrowImgName=@"homeScreenArrowImg";
    
    menuItemImages=[[NSMutableArray alloc]initWithObjects:@"agenda.png", nil];
    menuItemNames=[[NSMutableArray alloc]initWithObjects:@"Agenda", nil];
    
    _table.backgroundColor=[HexColorConvert colorWithHexString:tableBackgroundColor];
    _table.separatorColor=[HexColorConvert colorWithHexString:tableSeperatorColor];
    
    NSString *evenMenuScreenImagesPath = getMenuItemImagesPath(nil);
/*
//Adding left side navigation main creen button
    UIButton *homeButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 45, 45)];
    homeButton.backgroundColor=[UIColor clearColor];
    
    NSString *filePathForEvetLogoButton = [evenMenuScreenImagesPath stringByAppendingPathComponent:[NSString stringWithString:mainHomeScreenArrowImgName]];
    NSData *homeBtnImageData = [NSData dataWithContentsOfFile:filePathForEvetLogoButton];
    
    [homeButton setBackgroundImage:[UIImage imageWithData:homeBtnImageData] forState:UIControlStateNormal];
    UIBarButtonItem *homeBtn=[[UIBarButtonItem alloc]initWithCustomView:homeButton];
    [homeButton addTarget:self action:@selector(mainHomeButtonAction) forControlEvents:UIControlEventTouchUpInside];
    NSArray *arrayOfLeftSideItems=[[NSArray alloc]initWithObjects:homeBtn, nil];
    self.navigationItem.leftBarButtonItems=arrayOfLeftSideItems;


//Adding right
    UIImageView *imgNavigationEventLogo=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 45, 45)];
    imgNavigationEventLogo.backgroundColor=[UIColor clearColor];
    
    NSString *filePath = [evenMenuScreenImagesPath stringByAppendingPathComponent:[NSString stringWithString:eventNavigationLogo]];
    NSData *eventLogoImageData = [NSData dataWithContentsOfFile:filePath];
    
    imgNavigationEventLogo.image=[UIImage imageWithData:eventLogoImageData];
    UIBarButtonItem *logoItem=[[UIBarButtonItem alloc]initWithCustomView:imgNavigationEventLogo];
    
    NSArray *arrayOfItems=[[NSArray alloc]initWithObjects:logoItem, nil];
    self.navigationItem.rightBarButtonItems=arrayOfItems;
 */
    
    id firstTimeToHomesCreen = [[NSUserDefaults standardUserDefaults] objectForKey:@"FirstTimeToHomeScreen"];
    NSLog(@"Test1: %@",firstTimeToHomesCreen);
    NSInteger intVlaue=[firstTimeToHomesCreen integerValue];
    NSNumber *firstTimeLoadHomeScreen=[NSNumber numberWithInt:intVlaue];
    _imageViewMainMenuLogo.backgroundColor=[UIColor clearColor];
    
    if (![firstTimeLoadHomeScreen isEqualToNumber:[NSNumber numberWithInt:1]])
    {
       [self updateEventMenuScreen];
    }

    
    NSString *filePath = [evenMenuScreenImagesPath stringByAppendingPathComponent:[NSString stringWithString:eventNavigationLogo]];
    NSData *eventLogoImageData = [NSData dataWithContentsOfFile:filePath];
    
    _imageViewMainMenuLogo.image=[UIImage imageWithData:eventLogoImageData];

    
    
}

-(void)updateEventMenuScreen
{
    [self storeMenuItemImagesToFolder];
}



-(void)mainHomeButtonAction
{
    [self performSegueWithIdentifier:@"EnterToMainScreen" sender:self];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [menuItemNames count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *celID=@"MenuCell";
    Menu_Custom_TableViewCell *cell = (Menu_Custom_TableViewCell *)[tableView dequeueReusableCellWithIdentifier:celID];
    
    cell.lblMenuItemName.text=[menuItemNames objectAtIndex:indexPath.row];
    cell.lblMenuItemName.textColor=[HexColorConvert colorWithHexString:menuItemsNameColor];
    
    NSString *homeScreenImagesFolderPath = getMenuItemImagesPath(nil);
    NSString *filePath = [homeScreenImagesFolderPath stringByAppendingPathComponent:[NSString stringWithString:[menuItemImages objectAtIndex:0]]];
    NSData *menuImgData = [NSData dataWithContentsOfFile:filePath];

    cell.menuItemImageView.image=[UIImage imageWithData:menuImgData];
    
    return cell;

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     [self performSegueWithIdentifier:@"AgendaSegue" sender:self];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationItem.title=@"Menu";
    
    //    self.navigationItem.leftBarButtonItem.title=@"";
    //    [self.navigationItem.backBarButtonItem setTitle:@""];
}


#pragma mark- Method to store Menu Item Images in doccument directory.

-(void)storeMenuItemImagesToFolder
{
    NSString *menuItemFolderPath = getMenuItemImagesPath(nil);
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:menuItemFolderPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:menuItemFolderPath withIntermediateDirectories:NO attributes:nil error:Nil]; //Create folder
    }
    
   
    NSString *menuItemImageUrl= [NSString stringWithFormat:@"http://ieva.be/wp-content/uploads/2012/12/agenda-icon-150x150.png"];
    
    UIImage *menuItemImage = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:menuItemImageUrl]]];
    NSData *menuItemImgData = UIImagePNGRepresentation(menuItemImage);
    
    
    NSString *filePathForEnterBtnImage = [menuItemFolderPath stringByAppendingPathComponent:[menuItemImages objectAtIndex:0]];       //set save as image name.
   
    [menuItemImgData writeToFile:filePathForEnterBtnImage atomically:YES]; //Write the file
   
    NSString *eventLogoUrl= [NSString stringWithFormat:@"http://www2.psd100.com/ppp/2013/11/2701/Apple-1127161408.png"];
    
    UIImage *eventLogoImg = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:eventLogoUrl]]];
    NSData *eventLogoImgData = UIImagePNGRepresentation(eventLogoImg);
    
    
    NSString *filePathForEventLogo = [menuItemFolderPath stringByAppendingPathComponent:eventNavigationLogo];       //set save as image name.
    
    [eventLogoImgData writeToFile:filePathForEventLogo atomically:YES]; //Write the file

    NSString *homeArrowUrl= [NSString stringWithFormat:@"http://etc-mysitemyway.s3.amazonaws.com/icons/legacy-previews/icons/blue-tiedyed-cloth-icons-media/001986-blue-tiedyed-cloth-icon-media-a-media31-back.png"];
    
    UIImage *homeArrorImg = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:homeArrowUrl]]];
    NSData *homeArrowImgData = UIImagePNGRepresentation(homeArrorImg);
    
    
    NSString *filePathForHomeArrowImg = [menuItemFolderPath stringByAppendingPathComponent:mainHomeScreenArrowImgName];       //set save as image name.
    
    [homeArrowImgData writeToFile:filePathForHomeArrowImg atomically:YES]; //Write the file

    
}


#pragma mark - Method To get Menu Item Images Folder Path

NSString *getMenuItemImagesPath()
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *sponsorsFolder = [documentsPath stringByAppendingPathComponent:@"/MenuScreenImages/"];
    
    return sponsorsFolder;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
