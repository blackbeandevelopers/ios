//
//  HexColorConvert.h
//  MagentoApp
//
//  Created by Magneto on 1/25/14.
//  Copyright (c) 2014 JACK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HexColorConvert : NSObject
{
    
}
+(UIColor*)colorWithHexString:(NSString*)hex;
@end
