//
//  ListOfSpeakersCell.h
//  Nasscom_Application 1.0
//
//  Created by Black Bean Engagement on 24/02/14.
//  Copyright (c) 2014 Black Bean Engagement. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface ListOfSpeakersCell : UITableViewCell

@property (nonatomic,retain) IBOutlet UILabel *speakerName;
@property (nonatomic,retain) IBOutlet UILabel *speakerDesignation;
@property (nonatomic,retain) IBOutlet UIImageView *speakerImage;
@property (nonatomic,retain) IBOutlet AsyncImageView *asyncImageView;

@end
