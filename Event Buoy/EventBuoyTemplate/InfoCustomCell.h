//
//  InfoCustomCell.h
//  Nasscom_Application 1.0
//
//  Created by Black Bean Engagement on 14/02/14.
//  Copyright (c) 2014 Black Bean Engagement. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoCustomCell : UITableViewCell <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (nonatomic,retain) IBOutlet UILabel *infoTitleLabel;
@property (nonatomic,retain) IBOutlet UILabel *locationTitleLabel;
@property (nonatomic,retain) IBOutlet UILabel *categoryTitleLabel;
@property (nonatomic,retain) IBOutlet UILabel *decriptionTitleLabel;
@property (nonatomic,retain) IBOutlet UIImageView *rowArrowImage; // Image to Show Arro on Row
@property (nonatomic,retain) IBOutlet UIView *infoView;  // View To Show Info When Click on First Row.
@property (nonatomic,retain) IBOutlet UITableView *speakersTableView; // Table To show List of all Speakers
@property (nonatomic,retain) IBOutlet UIView *noteView; // text View To get Note From User.
@property (nonatomic,retain) IBOutlet UIButton *noteButton; // on Button click, will to take main text view page.
@property (nonatomic,retain) IBOutlet UITextView *noteTextView; // Text View To show note
@property (nonatomic,retain) IBOutlet UITextView *eventInfoTextView; // view to show info of event
@property (nonatomic, retain) IBOutlet UIImageView *cellImageBackground;


- (NSArray *)getSpeakerInfo : (NSArray *)SpeakerID; // Method to get all Info Of Speaker in an Array.
- (IBAction)onNoteButtonClick:(id)sender;
- (void)writeToFile :(NSNumber*)selectedID ; // Write Slelected Speaker ID to .plist file

@end
