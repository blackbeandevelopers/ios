//
//  Company_Website_ViewController.m
//  Event Buoy 2.0 Phase-1
//
//  Created by Alet Viegas on 05/06/14.
//  Copyright (c) 2014 Shailesh. All rights reserved.
//

#import "Company_Website_ViewController.h"

@interface Company_Website_ViewController ()
@property (nonatomic,retain)IBOutlet UIWebView *webView;
@property(nonatomic,retain)IBOutlet UIActivityIndicatorView *actLoad;

@property (weak, nonatomic) IBOutlet UILabel *labelScreenTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonMainMenu;

@end

extern NSString *getMenuItemImagesPath();

NSString *activityIndicatorColor;

NSString *titleTextColor;

NSString *mainMenuButtonImageName;

@implementation Company_Website_ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //self.navigationItem.title=@"Black Bean";
    
    titleTextColor=@"2981B3";
    
    mainMenuButtonImageName=@"eventlogo.png";
    
    activityIndicatorColor=@"2981B3";
    _actLoad.color=[HexColorConvert colorWithHexString:activityIndicatorColor];

    _labelScreenTitle.backgroundColor=[UIColor clearColor];
    _labelScreenTitle.textColor=[HexColorConvert colorWithHexString:titleTextColor];

    NSString *evenMenuScreenImagesPath = getMenuItemImagesPath(nil);
    
    _buttonMainMenu.backgroundColor=[UIColor clearColor];
    
    NSString *filePathForEvetLogoButton = [evenMenuScreenImagesPath stringByAppendingPathComponent:[NSString stringWithString:mainMenuButtonImageName]];
    NSData *eventLogoImageData = [NSData dataWithContentsOfFile:filePathForEvetLogoButton];
    
    [_buttonMainMenu setBackgroundImage:[UIImage imageWithData:eventLogoImageData] forState:UIControlStateNormal];

    
    NSURL *url=[NSURL URLWithString:@"http://blackbeanreyes.com/"];
    NSURLRequest *req=[NSURLRequest requestWithURL:url];
    [_webView loadRequest:req];
    
    
    
}



-(void)viewWillAppear:(BOOL)animated
{
//    self.navigationController.navigationBar.topItem.title = @"";
//    self.navigationItem.title=@"Black Bean";
    
}



- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [_actLoad startAnimating];
    
  
    [self.view bringSubviewToFront:_actLoad];
    _actLoad.hidden=NO;
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    [_actLoad stopAnimating];
    _actLoad.hidden=YES;
    
}

- (IBAction)buttonMainMenuAction:(id)sender
{
    [self performSegueWithIdentifier:@"EnterToMainMenuScreen" sender:self];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
