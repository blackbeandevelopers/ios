//
//  Agenda_ViewController.h
//  Event Buoy 2.0 Phase-1
//
//  Created by Alet Viegas on 05/06/14.
//  Copyright (c) 2014 Shailesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Agenda_ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *agendaTableView;
@end
