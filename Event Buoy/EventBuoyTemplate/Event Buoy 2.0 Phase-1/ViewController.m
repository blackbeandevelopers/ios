//
//  ViewController.m
//  Event Buoy 2.0 Phase-1
//
//  Created by Alet Viegas on 05/06/14.
//  Copyright (c) 2014 Shailesh. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic,retain)IBOutlet UIButton *btnEnterToEvent;     //Button for enter to event.
@property (nonatomic,retain)IBOutlet UIButton *btnCompanyLogo;     //Button for enter to event.
@property (retain,nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

NSString *navigationBarTextColor,*navigationBackBtnColor,*navigationBackGroundColor,*navigationBarTitle;
NSString *companyBtnLogoImage,*enterToEventBtnImage;

NSString *activityIndicatorColor;

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    id firstTimeToHomesCreen = [[NSUserDefaults standardUserDefaults] objectForKey:@"FirstTimeToHomeScreen"];
    NSLog(@"Test1: %@",firstTimeToHomesCreen);
    NSInteger intVlaue=[firstTimeToHomesCreen integerValue];
    NSNumber *firstTimeLoadHomeScreen=[NSNumber numberWithInt:intVlaue];

    navigationBackBtnColor=@"FFFFFF";
    navigationBackGroundColor=@"2981B3";
    navigationBarTitle=@"Event Buoy Application";
    navigationBarTextColor=@"FFFFFF";
    
    companyBtnLogoImage=@"companylogo.png";
    enterToEventBtnImage=@"enterbutton.png";
    
    self.navigationController.navigationBar.barTintColor = [HexColorConvert colorWithHexString:navigationBackGroundColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [HexColorConvert colorWithHexString:navigationBarTextColor]};
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationController.navigationBar.tintColor = [HexColorConvert colorWithHexString:navigationBackBtnColor];
    self.navigationItem.title=navigationBarTitle;
    
    
    activityIndicatorColor=@"2981B3";
    _activityIndicator.color=[HexColorConvert colorWithHexString:activityIndicatorColor];
    _activityIndicator.hidesWhenStopped=YES;
    

    [self createEnterToEventButton];           //Create 'Cleck Here To Enter' button on main home screen.
    [self createCompanyLogoButton];            //Create 'Compony Logo' at right bottom on main home screen.
    
    if (![firstTimeLoadHomeScreen isEqualToNumber:[NSNumber numberWithInt:1]])
    {
        [self updateHomeScree];
        
    }

}


-(void)updateHomeScree
{
    
    [self storeHomeScreenImagesToFolder];       //Call method to download and store.
    
    [self createDatabase];
   
    NSNumber *secondTime=[NSNumber numberWithInt:1];
    [[NSUserDefaults standardUserDefaults] setObject:secondTime forKey:@"FirstTimeToHomeScreen"];


}


#pragma mark- Method to create 'Click here to enter' button on flash screen.

-(void)createEnterToEventButton
{
   // _btnEnterToEvent=[[UIButton alloc]initWithFrame:CGRectMake(95, 100, 130, 130)];
    _btnEnterToEvent.backgroundColor=[UIColor clearColor];
    
    NSString *homeScreenImagesFolderPath = getHomeScreenImagesPath(nil);
    NSString *filePath = [homeScreenImagesFolderPath stringByAppendingPathComponent:[NSString stringWithString:enterToEventBtnImage]];
    NSData *pngData = [NSData dataWithContentsOfFile:filePath];
    [_btnEnterToEvent setBackgroundImage:[UIImage imageWithData:pngData] forState:UIControlStateNormal];
    [_btnEnterToEvent addTarget:self action:@selector(enterToEventButtonAction) forControlEvents:UIControlEventTouchUpInside];
   // [self.view addSubview:_btnEnterToEvent];
    
}

#pragma mark- Method to create 'CompanyLogoButton' button on flash screen.

-(void)createCompanyLogoButton
{
    _btnCompanyLogo=[[UIButton alloc]initWithFrame:CGRectMake(235, 331, 70, 70)];
    _btnCompanyLogo.backgroundColor=[UIColor clearColor];
    NSString *homeScreenImagesFolderPath = getHomeScreenImagesPath(nil);
    NSString *filePath = [homeScreenImagesFolderPath stringByAppendingPathComponent:[NSString stringWithString:companyBtnLogoImage]];
    NSData *pngData = [NSData dataWithContentsOfFile:filePath];
    [_btnCompanyLogo setBackgroundImage:[UIImage imageWithData:pngData] forState:UIControlStateNormal];
    [_btnCompanyLogo addTarget:self action:@selector(companyButtonPressAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnCompanyLogo];
    
}


- (void)createDatabase   // 1. Create Database when Home Page Launch First Time
{
    NSString *databasePath=getFilePath();
 //   NSLog(@"%@",databasePath);
    sqlite3 *database;
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            
            char *errMsg;
            const char *sql_stmt;
            sql_stmt = "CREATE TABLE IF NOT EXISTS EVENT (ID INTEGER PRIMARY KEY,NAME TEXT,DAY TEXT, DATE TEXT, TIME TEXT, LOCATION TEXT,CATEGORY TEXT,SPEAKERS_ID TEXT,DESCRIPTION TEXT, EVENT_ORDER INTEGER)";
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
                NSLog(@"Fail to create database");
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS SPEAKERS (ID INTEGER PRIMARY KEY,NAME TEXT,DETAILS1 TEXT, DETAILS2 TEXT, IMAGE TEXT, INFO TEXT,SESSIONS TEXT,SOCIAL TEXT, SPEAKER_ORDER INTEGER)";
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
                NSLog(@"Fail to create database");
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS SPONSORS (ID INTEGER PRIMARY KEY,NAME TEXT,TYPE TEXT, LOGO TEXT, SPONSOR_URL TEXT, SPONSOR_ORDER INTEGER)";
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
                NSLog(@"Fail to create database");
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS NOTE (ID INTEGER PRIMARY KEY,NOTE TEXT,FOREIGN KEY (ID) REFERENCES EVENT(ID))";
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
                NSLog(@"Fail to create database");
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS EVENT_STATUS (ID INTEGER PRIMARY KEY,STATUS INTEGER,FOREIGN KEY (ID) REFERENCES EVENT(ID))";
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
                NSLog(@"Fail to create database");
            
            sqlite3_close(database);
        }
        else
            NSLog(@"Fail to open database");
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationItem.title=navigationBarTitle;
    self.navigationItem.hidesBackButton = YES;
    
}




-(void)enterToEventButtonAction
{
    //_activityIndicator.hidesWhenStopped=YES;
    [_activityIndicator startAnimating];
    [self performSegueWithIdentifier:@"EnterToEventMenu" sender:self];
    
}

-(void)companyButtonPressAction
{
    [self performSegueWithIdentifier:@"CompanyWebsiteViewController" sender:self];
}



#pragma mark- Method to store Home Screen Images in doccument directory.

-(void)storeHomeScreenImagesToFolder
{
    NSString *homeScreenImagesFolderPath = getHomeScreenImagesPath(nil);
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:homeScreenImagesFolderPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:homeScreenImagesFolderPath withIntermediateDirectories:NO attributes:nil error:Nil]; //Create folder
    }
   
    
        NSString *imageUrlString= [NSString stringWithFormat:@"http://us.cdn1.123rf.com/168nwm/arcady31/arcady311101/arcady31110100002/8623319-click-here-glass-icon.jpg"];
    
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrlString]]];
        NSData *pngData = UIImagePNGRepresentation(image);
    
        NSString *companyLogoUrl= [NSString stringWithFormat:@"http://www.socialsamosa.com/wp-content/uploads/2014/02/BBE-Logo.png"];
        
        UIImage *companyLogo = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:companyLogoUrl]]];
        NSData *companyLogoData = UIImagePNGRepresentation(companyLogo);

    
        NSLog(@"%@",homeScreenImagesFolderPath);
        NSString *filePathForEnterBtnImage = [homeScreenImagesFolderPath stringByAppendingPathComponent:enterToEventBtnImage];       //set save as image name.
        NSString *filePathForCompanyLogo = [homeScreenImagesFolderPath stringByAppendingPathComponent:companyBtnLogoImage];
        [pngData writeToFile:filePathForEnterBtnImage atomically:YES]; //Write the file
        [companyLogoData writeToFile:filePathForCompanyLogo atomically:YES]; //Write the file

    
    
    
}


#pragma mark - Method To get Home screen Folder Path

NSString *getHomeScreenImagesPath()
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *sponsorsFolder = [documentsPath stringByAppendingPathComponent:@"/HomeScreenImages/"];
    
    return sponsorsFolder;
}


NSString *getFilePath()
{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"Event_Buoy.db"]];
    NSLog(@"%@",databasePath);
    return databasePath;
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
