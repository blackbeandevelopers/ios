//
//  Agenda_ViewController.m
//  Event Buoy 2.0 Phase-1
//
//  Created by Alet Viegas on 05/06/14.
//  Copyright (c) 2014 Shailesh. All rights reserved.
//

#import "Agenda_ViewController.h"
#import "Agenda_TableViewCell.h"

@interface Agenda_ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblAgendaDate;
@property (weak, nonatomic) IBOutlet UIButton *btnAgendaBackButton;
@property (weak, nonatomic) IBOutlet UIButton *btnAgendaNextButton;
@property (weak, nonatomic) IBOutlet UISearchBar *agendaSearchBar;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewAgenda;
@property (retain,nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *showMenuButton;
@property (weak, nonatomic) IBOutlet UILabel *agendaTitleLabel;

@property (retain, nonatomic) NSArray *listOfSpeakers;

- (IBAction)backButtonClickAction:(id)sender;
- (IBAction)nextButtonClickAction:(id)sender;

@end

extern NSString *getMenuItemImagesPath();

extern NSString *getFilePath();
extern NSString *getSpeakerImagesFilePath(); // Method to Store Images of Speakers
NSMutableArray *_agendaRecordArray;  // Array to get All recods of Database
NSArray *agendaRecordReloadArray; // Array to Reload at a time of navigation.
NSArray *eventStatusArray; // Array to Hold Status of Each Event.
NSDictionary *eventRecordDictionary; // Dictionary to Store Slected Event Record.
NSArray *listOfDatesArray;   // Array to Get List of Dates and Days.
NSString *Date;   // Selected Date

NSString *eventNavigationLogo;

int rowSelected = 0; // Event Row Selected to add to my scheduled
BOOL statusOfEvent =0; // Scheduled Status of Event
 int speakerFlag; // Flag to access speakers data only once

extern NSMutableArray *menuItemImages,*menuItemNames;

NSString *backBtnImageName,*nextBtnImageName;
NSString *agendaDateTextColor,*agendaDateBgColor;

NSString *activityIndicatorColor;

NSString *addImgName,*tickMrakImgName;

NSString *agendaTableViewSeperatorColor;
NSString *agendaTitleTextColor;
NSString *agendaVenueTextColor;
NSString *agendaTimeTextColor;


NSString *mainMenuButtonImageName;

int pageIndex=0;
NSArray *agendaRecordReloadArray;
NSArray *listOfDatesArray;
NSString *Date;
int totalNumberOfDates = 0;

@implementation Agenda_ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationItem.title=@"Agenda";
    
    agendaDateBgColor=@"FFFFFF";
    agendaDateTextColor=@"2981B3";
    
    activityIndicatorColor=@"2981B3";
    
    agendaTitleTextColor=@"2981B3";
    agendaVenueTextColor=@"2981B3";
    agendaTimeTextColor=@"2981B3";
    
    agendaTableViewSeperatorColor=@"2981B3";
    
    backBtnImageName=@"Left-arrow.png";
    nextBtnImageName=@"right-arrow.png";
    
    addImgName=@"addImage.png";
    tickMrakImgName=@"tickMrakImage.png";
    
    eventNavigationLogo=@"menuButtonImage.png";
    mainMenuButtonImageName=@"eventlogo.png";
    
    NSString *evenMenuScreenImagesPath = getMenuItemImagesPath(nil);
    
    UIImageView *imgNavigationMenuItemLogo=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 45, 45)];
    imgNavigationMenuItemLogo.backgroundColor=[UIColor clearColor];

    NSString *filePathForMenuItemIcon = [evenMenuScreenImagesPath stringByAppendingPathComponent:[NSString stringWithString:[menuItemImages objectAtIndex:0]]];
    NSData *eventMenuImageData = [NSData dataWithContentsOfFile:filePathForMenuItemIcon];
    
    imgNavigationMenuItemLogo.image=[UIImage imageWithData:eventMenuImageData];
    UIBarButtonItem *logoItem=[[UIBarButtonItem alloc]initWithCustomView:imgNavigationMenuItemLogo];
    
    NSArray *arrayOfItems=[[NSArray alloc]initWithObjects:logoItem, nil];
    self.navigationItem.leftBarButtonItems=arrayOfItems;

//Adding right side navigation event logo
    UIButton *imgNavigationEventLogoButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 45, 45)];
    imgNavigationEventLogoButton.backgroundColor=[UIColor clearColor];
    
    NSString *filePathForEvetLogoButton = [evenMenuScreenImagesPath stringByAppendingPathComponent:[NSString stringWithString:eventNavigationLogo]];
    NSData *eventLogoImageData = [NSData dataWithContentsOfFile:filePathForEvetLogoButton];
    
    [imgNavigationEventLogoButton setBackgroundImage:[UIImage imageWithData:eventLogoImageData] forState:UIControlStateNormal];
    UIBarButtonItem *evetLogoButton=[[UIBarButtonItem alloc]initWithCustomView:imgNavigationEventLogoButton];
    [imgNavigationEventLogoButton addTarget:self action:@selector(homeButtonAction) forControlEvents:UIControlEventTouchUpInside];
    NSArray *arrayOfRightSideItems=[[NSArray alloc]initWithObjects:evetLogoButton, nil];
    self.navigationItem.rightBarButtonItems=arrayOfRightSideItems;
    
    NSString *agendaScreeImageFolderPath = getAgendaScreenImagesPath(nil);
    
   NSString *filePathForBackBtnImage = [agendaScreeImageFolderPath stringByAppendingPathComponent:[NSString stringWithString:backBtnImageName]];
    NSData *backBntImgData = [NSData dataWithContentsOfFile:filePathForBackBtnImage];
    
    NSString *filePathForNextBtnImage = [agendaScreeImageFolderPath stringByAppendingPathComponent:[NSString stringWithString:nextBtnImageName]];
    NSData *nextBtnImgData = [NSData dataWithContentsOfFile:filePathForNextBtnImage];
    
    [_btnAgendaBackButton setBackgroundImage:[UIImage imageWithData:backBntImgData] forState:UIControlStateNormal];
    [_btnAgendaNextButton setBackgroundImage:[UIImage imageWithData:nextBtnImgData] forState:UIControlStateNormal];
    
    _activityIndicator.color=[HexColorConvert colorWithHexString:activityIndicatorColor];
    
    _agendaTableView.separatorColor=[HexColorConvert colorWithHexString:agendaTableViewSeperatorColor];
    _lblAgendaDate.backgroundColor=[HexColorConvert colorWithHexString:agendaDateBgColor];
    _lblAgendaDate.textColor=[HexColorConvert colorWithHexString:agendaDateTextColor];
    _agendaTitleLabel.textColor=[HexColorConvert colorWithHexString:agendaTitleTextColor];
    
    NSString *backDateBtnImg = getAgendaScreenImagesPath(nil);
    NSString *filePath = [backDateBtnImg stringByAppendingPathComponent:[NSString stringWithString:backBtnImageName]];
    NSData *backDateBtnImgData = [NSData dataWithContentsOfFile:filePath];
    [_btnAgendaBackButton setBackgroundImage:[UIImage imageWithData:backDateBtnImgData] forState:UIControlStateNormal];
    
    
    filePathForEvetLogoButton = [evenMenuScreenImagesPath stringByAppendingPathComponent:[NSString stringWithString:mainMenuButtonImageName]];
    eventLogoImageData = [NSData dataWithContentsOfFile:filePathForEvetLogoButton];
    
    [_showMenuButton setBackgroundImage:[UIImage imageWithData:eventLogoImageData] forState:UIControlStateNormal];

    _agendaRecordArray =[self getAgendaRecords]; // 1. getting Records in an Array from Database -
    
    _activityIndicator.hidesWhenStopped=YES;
    [_btnAgendaBackButton setEnabled:NO];
    [_btnAgendaNextButton setEnabled:NO];
    
    //  [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"action-bar320-65.png"] forBarMetrics:UIBarMetricsDefault];
    
    id jsonAgenda = [[NSUserDefaults standardUserDefaults] objectForKey:@"JsoncountAgenda"];
    NSLog(@"Test1: %@",jsonAgenda);
    NSInteger intVlaueAgenda=[jsonAgenda integerValue];
    NSNumber *jsonCountForAgenda=[NSNumber numberWithInt:intVlaueAgenda];
    
    NSLog(@"json count : %@",jsonCountForAgenda);
    NSLog(@"db count : %d",[_agendaRecordArray count]);
    if (![jsonCountForAgenda isEqualToNumber:[NSNumber numberWithInt:[_agendaRecordArray count]]] || [_agendaRecordArray count]==0 )       // if no data found      // we are fetching agenda and speakers at same time on agenda main pages
    {
        Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
        
        if (networkStatus == NotReachable)
        {
            UIAlertView *networkAlert= [[UIAlertView alloc]initWithTitle:@"Network Alert" message:@"Please Connect to Internet To access this Service " delegate:Nil cancelButtonTitle:@"ok" otherButtonTitles:Nil, nil];
            [networkAlert show];
        }
        else
        {
            [_activityIndicator startAnimating];
            NSURL *url=[NSURL URLWithString:@"http://listoftablets.com/tiecon/admin/getEvents.php"];
            dispatch_async(kBgQueue, ^{
                
                [self storeAgendaListScreenToFolder];
                
                NSData* data = [NSData dataWithContentsOfURL : url];
                
                NSDictionary* json = [NSJSONSerialization
                                      JSONObjectWithData:data //1
                                      options:kNilOptions
                                      error:Nil];
                
                NSMutableArray* events = [json objectForKey:@"event_details"]; //2
                
                NSNumber *jsonCountAgenda=[NSNumber numberWithInt:[events count]];
                [[NSUserDefaults standardUserDefaults] setObject:jsonCountAgenda forKey:@"JsoncountAgenda"];
                
                
                [self AddEntries:events];  // Adding Fetched Agenda to Database - we have to Show events by order so we first inserting values in database.
                _agendaRecordArray = [self getAgendaRecords]; // getting Records in an Array from Database
                [self getDataInArray];  // Load Total Days for Events, Show back and Next button, etc.
                
                [_agendaTableView reloadData];
                
                dispatch_async(dispatch_get_main_queue(), ^(void)
                               {
                                   NSLog(@"on main thread..");
                                   [_agendaTableView reloadData];
                                   [_lblAgendaDate setNeedsDisplay];
                                   [_activityIndicator stopAnimating];
                                   
                               });
                
                if (speakerFlag==0)
                {
                    speakerFlag=1;
                    [self speakersOperation];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^(void)
                               {
                                   NSLog(@"on main thread..");
                                   
                                   
                               });
                
            });
            
        }
    }
    else
    {
        [self getDataInArray];
    }

    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationItem.title=@"Agenda";
    
}

-(void)homeButtonAction
{
    [self performSegueWithIdentifier:@"EnterToEventMenu" sender:self];
}

- (void)speakersOperation
{
    NSURL *urlSpeakers=[NSURL URLWithString:@"http://listoftablets.com/tiecon/admin/getSpeakers.php"];
    NSData* speakerData = [NSData dataWithContentsOfURL : urlSpeakers];
    
    NSLog(@"%@",speakerData);
    
    NSDictionary* speakerJson = [NSJSONSerialization
                                 JSONObjectWithData:speakerData // 1
                                 
                                 options:kNilOptions
                                 error:Nil];
    
    NSArray* speakers = [speakerJson objectForKey:@"speakers_detais"]; //2
    NSNumber *jsonSpeakers=[NSNumber numberWithInt:[speakers count]];
    [[NSUserDefaults standardUserDefaults] setObject:jsonSpeakers forKey:@"JsoncountSpeakers"];
    [self AddEntriesSpeakers:speakers];
    _listOfSpeakers = [self getListOfSpeakers]; // getting Records in an Array from Database
    [self loadImagesToFiles];  // Load all speaker Images
    
}


#pragma mark - Method to get List Of Speakers in an Array.

- (NSArray *)getListOfSpeakers
{
    
    NSMutableArray *speakerArray = [[NSMutableArray alloc]init];
    
    
    NSString *databasePath=getFilePath();
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    sqlite3 *database;
    
    if ([filemgr fileExistsAtPath: databasePath])
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            NSString *sqlStatement = [NSString stringWithFormat:@"SELECT * FROM SPEAKERS ORDER BY SPEAKER_ORDER"];
            
            sqlite3_stmt *statement;
            
            if(sqlite3_prepare_v2(database, [sqlStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
            {
                while(sqlite3_step(statement) == SQLITE_ROW)
                {
                    NSInteger ID=sqlite3_column_int(statement, 0);
                    NSString *NAME=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,1)];
                    NSString *DETAILS1=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,2)];
                    NSString *DETAILS2=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,3)];
                    NSString *IMAGE=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,4)];
                    NSString *INFO=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,5)];
                    NSString *SESSIONS=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,6)];
                    NSString *SOCIAL=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,7)];
                    
                    
                    NSDictionary *recordDictionary=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:ID],@"ID",NAME,@"NAME",DETAILS1,@"DETAILS1",DETAILS2,@"DETAILS2",IMAGE,@"IMAGE",INFO,@"INFO",SESSIONS,@"SESSIONS",SOCIAL,@"SOCIAL",Nil];      // getting record on dictionary
                    
                    [speakerArray addObject:recordDictionary];   // Adding Each Record in an Array
                }
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    return speakerArray;
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [agendaRecordReloadArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *celID=@"AgendaCell";
    Agenda_TableViewCell *cell = (Agenda_TableViewCell *)[tableView dequeueReusableCellWithIdentifier:celID];
    
    NSDictionary *agendaRecodsDict = [agendaRecordReloadArray objectAtIndex:indexPath.row];   //  getting Entries in the Dictionary
    NSLog(@"%@",agendaRecordReloadArray);
    NSLog(@"%@",agendaRecodsDict);
    
    cell.btnScheduleTrack.backgroundColor=[UIColor clearColor];
    
    cell.lblAgendaTitle.text = [agendaRecodsDict objectForKey:@"NAME"];
    cell.lblAgendaTitle.textColor=[HexColorConvert colorWithHexString:agendaTitleTextColor];

    cell.lblAgendaTime.text = [agendaRecodsDict objectForKey:@"TIME"];
    cell.lblAgendaTime.textColor=[HexColorConvert colorWithHexString:agendaTimeTextColor];
    
    NSString *agendaScreeImageFolderPath = getAgendaScreenImagesPath(nil);
    NSString *filePathForAddImage = [agendaScreeImageFolderPath stringByAppendingPathComponent:[NSString stringWithString:addImgName]];
    NSData *addImgData = [NSData dataWithContentsOfFile:filePathForAddImage];
    
    NSString *filePathFortickMrakImage = [agendaScreeImageFolderPath stringByAppendingPathComponent:[NSString stringWithString:tickMrakImgName]];
    NSData *tickMrakImgData = [NSData dataWithContentsOfFile:filePathFortickMrakImage];
    

    
    int eventID = [[agendaRecodsDict objectForKey:@"ID"]integerValue];
    
    if ([eventStatusArray count]==0)    // if event Status table is null then place unmark image in image view
    {
        [cell.btnScheduleTrack setBackgroundImage:[UIImage imageWithData:addImgData] forState:UIControlStateNormal];
    }
    else    // chcek that event is added in an schedule
    {
        NSNumber *scheduledFlag = 0;
        
        for (NSDictionary * eventDict in eventStatusArray) // Search for Dictionary in an array
        {
            int ID = [[eventDict valueForKey:@"ID"]integerValue];    //
            if (ID==eventID)
            {
                NSLog(@"%@",eventDict);
                scheduledFlag = [eventDict valueForKey:@"STATUS"];
                break;
            }
        }
        
        int flag = [scheduledFlag intValue];
        
        if (flag == 1)
           [cell.btnScheduleTrack setBackgroundImage:[UIImage imageWithData:tickMrakImgData] forState:UIControlStateNormal];
        else
            [cell.btnScheduleTrack setBackgroundImage:[UIImage imageWithData:addImgData] forState:UIControlStateNormal];
        
    }

   
    return cell;
    
}


#pragma mark - Table View Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d",indexPath.row);
    eventRecordDictionary = [[NSDictionary  alloc]initWithDictionary:[agendaRecordReloadArray objectAtIndex:indexPath.row]];
    NSLog(@"%@",eventRecordDictionary);
    
    NSNumber *selectedEventID = [eventRecordDictionary objectForKey:@"ID"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"Tiecon.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: path])
    {
        path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat: @"Tiecon.plist"] ];
    }
    
    NSMutableDictionary *data;
    
    if ([fileManager fileExistsAtPath: path])
    {
        data = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    }
    else
    {
        // If the file doesn’t exist, create an empty dictionary
        data = [[NSMutableDictionary alloc] init];
    }
    
    //To insert the data into the plist
    [data setObject:selectedEventID forKey:@"SelectedEvent"];
    [data writeToFile:path atomically:YES];
    
    [self performSegueWithIdentifier:@"EventSegue" sender:self];    // Move to Event Detail Page
}




- (void)getDataInArray
{
    eventStatusArray = [[NSArray alloc]init];
    
    eventStatusArray = [self getStatusOfEvent];
    
    NSLog(@"%@",eventStatusArray);
    
    listOfDatesArray = [self getTotalDates];     // Get List of Dates and day in an Array
    
    NSLog(@"%@",listOfDatesArray);
    
    [listOfDatesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSLog(@"%@",listOfDatesArray);
    
    totalNumberOfDates = [listOfDatesArray count]/2; // Get Total Number of Dates.
    
    NSLog(@"%@",listOfDatesArray);
    
    if (listOfDatesArray.count != 0)
    {
        [self showDateAndDay];   // 1.show Dates and Day  2.Reload Table View Entries as per selected date
    }
    if (pageIndex == 0)
    {
        _btnAgendaBackButton.enabled=NO;
    }
    if (totalNumberOfDates<=1)
    {
        _btnAgendaNextButton.enabled=NO;
    }
    
}

#pragma mark - Array to hold Information About Event Status to check that is event added to my scheduled.

- (NSArray *)getStatusOfEvent
{
    NSMutableArray *getRecodsArray = [[NSMutableArray alloc]init];
    
    NSString *databasePath=getFilePath();
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    sqlite3 *database;
    
    if ([filemgr fileExistsAtPath: databasePath])
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            const char *sqlStatement = "SELECT * FROM EVENT_STATUS";
            
            sqlite3_stmt *statement;
            
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK)
            {
                while(sqlite3_step(statement) == SQLITE_ROW)
                {
                    NSInteger ID=sqlite3_column_int(statement, 0);
                    NSInteger STATUS=sqlite3_column_int(statement, 1);
                    
                    NSDictionary *recordDictionary=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:ID],@"ID",[NSNumber numberWithInt:STATUS],@"STATUS",Nil];      // getting record on dictionary
                    
                    [getRecodsArray addObject:recordDictionary];   // Adding Each Record in an Array
                }
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    return getRecodsArray;
    
    
}

# pragma mark - Method To get All Recods of Agenda

- (NSMutableArray*)getAgendaRecords
{
    NSMutableArray *getRecodsArray = [[NSMutableArray alloc]init];
    
    NSString *databasePath=getFilePath();
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    sqlite3 *database;
    
    if ([filemgr fileExistsAtPath: databasePath])
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            const char *sqlStatement = "SELECT * FROM EVENT ORDER BY EVENT_ORDER";
            
            sqlite3_stmt *statement;
            
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK)
            {
                while(sqlite3_step(statement) == SQLITE_ROW)
                {
                    NSInteger ID=sqlite3_column_int(statement, 0);
                    NSString *NAME=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,1)];
                    NSString *DAY=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,2)];
                    NSString *DATE=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,3)];
                    NSString *TIME=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,4)];
                    NSString *LOCATION=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,5)];
                    NSString *CATEGORY=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,6)];
                    NSString *SPEAKERS_ID=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,7)];
                    NSString *DESCRIPTION=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,8)];
                    
                    NSDictionary *recordDictionary=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:ID],@"ID",NAME,@"NAME",DAY,@"DAY",DATE,@"DATE",TIME,@"TIME",LOCATION,@"LOCATION",CATEGORY,@"CATEGORY",SPEAKERS_ID,@"SPEAKERS_ID",DESCRIPTION,@"DESCRIPTION",Nil];      // getting record on dictionary
                    
                    [getRecodsArray addObject:recordDictionary];   // Adding Each Record in an Array
                }
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    return getRecodsArray;
}

#pragma mark - Method To write Fetched Data To Database
- (void)AddEntriesSpeakers : (NSArray*)speakerDataArray
{
    NSString *databasePath=getFilePath();
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    sqlite3 *database;
    sqlite3_stmt *statement = NULL;
    
    if ([filemgr fileExistsAtPath: databasePath])
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            NSString *insertSQL=[[NSString alloc]init];
            
            for (int i=0; i < [speakerDataArray count]; i++)
            {
                NSString *details1 = [[speakerDataArray objectAtIndex:i] valueForKey:@"speaker_detail1"];
                NSLog(@"%@",details1);
                
                NSString *details2 = [[speakerDataArray objectAtIndex:i] valueForKey:@"speaker_detail2"];
                NSLog(@"%@",details2);
                
                NSString *url = [[speakerDataArray objectAtIndex:i] valueForKey:@"speaker_image_url"];
                NSLog(@"%@",url);
                
                NSString *info = [[speakerDataArray objectAtIndex:i] valueForKey:@"speaker_info"];
                NSLog(@"%@",info);
                info = [info stringByReplacingOccurrencesOfString:@"'" withString:@" "];
                info = [info stringByReplacingOccurrencesOfString:@"/" withString:@" "];
                
                NSLog(@"%@",info);
                
                NSString *sessions = [[speakerDataArray objectAtIndex:i] valueForKey:@"speaker_sessions"];
                NSLog(@"%@",details2);
                
                NSString *social = [[speakerDataArray objectAtIndex:i] valueForKey:@"social"] ;
                NSLog(@"%@",social);
                NSString *speakerOrder = [[speakerDataArray objectAtIndex:i] valueForKey:@"speaker_order"];
                
                insertSQL = [NSString stringWithFormat: @"REPLACE INTO SPEAKERS (ID ,NAME ,DETAILS1 , DETAILS2 , IMAGE , INFO ,SESSIONS ,SOCIAL ,SPEAKER_ORDER) VALUES ('%@','%@','%s','%s','%s','%s','%s','%s','%s')",[[speakerDataArray objectAtIndex:i] valueForKey:@"speaker_id"],[[speakerDataArray objectAtIndex:i] valueForKey:@"speaker_name"],[details1 UTF8String],[details2 UTF8String],[url UTF8String],[info UTF8String],[sessions UTF8String],[social UTF8String],[speakerOrder UTF8String]];
                
                const char *insert_stmt = [insertSQL UTF8String];
                char *errMsg = NULL;
                
                sqlite3_prepare_v2(database, insert_stmt, -1, &statement,Nil);
                
                if (sqlite3_exec(database, insert_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
                {
                    
                }
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    
                }
                else
                    NSLog(@"error");
                
            }
            
            sqlite3_finalize(statement);
            
            sqlite3_close(database);
            
        }
    }
    
}



- (void)AddEntries : (NSArray *)eventDataArray
{
    NSLog(@"%@",eventDataArray);
    
    NSString *databasePath=getFilePath();
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    sqlite3 *database;
    sqlite3_stmt *statement;
    
    if ([filemgr fileExistsAtPath: databasePath])
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            NSString *insertSQL=[[NSString alloc]init];
            
            for (int i=0; i < [eventDataArray count]; i++)
            {
                NSLog(@"%@",[eventDataArray objectAtIndex:i]);
                
                NSString *description = [[eventDataArray objectAtIndex:i] valueForKey:@"event_description"];
                NSLog(@"%@",description);
                description = [description stringByReplacingOccurrencesOfString:@"'" withString:@" "];
                description = [description stringByReplacingOccurrencesOfString:@"/" withString:@" "];
                
                insertSQL = [NSString stringWithFormat: @"REPLACE INTO EVENT(ID ,NAME ,DAY , DATE , TIME , LOCATION ,CATEGORY ,SPEAKERS_ID ,DESCRIPTION , EVENT_ORDER ) VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",[[eventDataArray objectAtIndex:i] valueForKey:@"event_id"],[[eventDataArray objectAtIndex:i] valueForKey:@"event_name"],[[eventDataArray objectAtIndex:i] valueForKey:@"event_day"],[[eventDataArray objectAtIndex:i] valueForKey:@"event_date"],[[eventDataArray objectAtIndex:i] valueForKey:@"event_time"],[[eventDataArray objectAtIndex:i] valueForKey:@"event_location"],[[eventDataArray objectAtIndex:i] valueForKey:@"event_category"],[[eventDataArray objectAtIndex:i] valueForKey:@"event_speaker_ids"],description,[[eventDataArray objectAtIndex:i] valueForKey:@"event_order"]];
                
                const char *insert_stmt = [insertSQL UTF8String];
                
                sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL);
                
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    
                }
                else
                    NSLog(@"error");
            }
            
        }
        
        sqlite3_finalize(statement);
        
        sqlite3_close(database);
    }
    
}

#pragma mark - Method to show Day and Date

- (void)showDateAndDay     // 1.Show Day and Date Name    2.Reload Table View as per Date Selected
{
    Date = [listOfDatesArray objectAtIndex:pageIndex*2];
    NSString *Day = [listOfDatesArray objectAtIndex:pageIndex*2+1];
    
    NSLog(@"%@",Date);
    NSLog(@"%@",Day);
    
    NSLog(@"%d",pageIndex);
    NSLog(@"%d",[listOfDatesArray count]);
    
    _btnAgendaBackButton.enabled=YES;
    _btnAgendaNextButton.enabled = YES;
    
    if (pageIndex==0)
    {
        _btnAgendaBackButton.enabled = NO;
    }
    else if (pageIndex==[listOfDatesArray count]/2-1)
    {
        _btnAgendaNextButton.enabled = NO;
    }
    
    Day = [Day substringToIndex:3];
    NSString *date = [Date substringToIndex:5];
    
    _lblAgendaDate.text = [NSString stringWithFormat:@"%@ %@",Day,date];
    
    agendaRecordReloadArray = [self getSortedArray:Date];
    
    NSLog(@"%@",agendaRecordReloadArray);
    
}

#pragma mark - Method to get Sorted Array as per selected Date

- (NSMutableArray*)getSortedArray:(NSString *)selectedDate
{
    NSMutableArray *sortedArray = [[NSMutableArray alloc]init];
    
    NSLog(@"%@",[[_agendaRecordArray objectAtIndex:0]valueForKey:@"DATE"]);
    
    for (int i=0; i<[_agendaRecordArray count]; i++)
    {
        NSString * Date = [[_agendaRecordArray objectAtIndex:i] valueForKey:@"DATE"];
        
        if ([Date isEqualToString:selectedDate])
        {
            [sortedArray addObject:[_agendaRecordArray objectAtIndex:i]];
        }
    }
    return sortedArray;
}


#pragma mark - Method To get Total Number of Dates

- (NSMutableArray*)getTotalDates
{
    NSMutableArray *dateArrayName= [[NSMutableArray alloc]init];    // List of Total Dates Given
    
    for (int i=0; i < [_agendaRecordArray count]; i++)
    {
        NSDictionary *agendaRecodsDict = [_agendaRecordArray objectAtIndex:i];
        NSString *dateString = [agendaRecodsDict objectForKey:@"DATE"];
        NSString *dayString = [agendaRecodsDict objectForKey:@"DAY"];
        
        if (![dateArrayName containsObject:dateString])
        {
            [dateArrayName addObject:dateString];
            [dateArrayName addObject:dayString];
        }
    }
    
    return dateArrayName;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark- Method to store Menu Item Images in doccument directory.

-(void)storeAgendaListScreenToFolder
{
    NSString *agendaScreenImgFolderPath = getAgendaScreenImagesPath(nil);
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:agendaScreenImgFolderPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:agendaScreenImgFolderPath withIntermediateDirectories:NO attributes:nil error:Nil]; //Create folder
    }
    
//BackButton image store
    NSString *backButtonUrl= [NSString stringWithFormat:@"http://ieva.be/wp-content/uploads/2012/12/agenda-icon-150x150.png"];
    
    UIImage *backBtnImg = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:backButtonUrl]]];
    NSData *bkBtnImgData = UIImagePNGRepresentation(backBtnImg);
    
    
    NSString *filePathForBkBtn = [agendaScreenImgFolderPath stringByAppendingPathComponent:backBtnImageName];       //set save as image name.
    
    [bkBtnImgData writeToFile:filePathForBkBtn atomically:YES]; //Write the file

//NextButton image store
    NSString *nextBtUrl= [NSString stringWithFormat:@"http://www2.psd100.com/ppp/2013/11/2701/Apple-1127161408.png"];
    
    UIImage *nextBtImg = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:nextBtUrl]]];
    NSData *nextBtnImgData = UIImagePNGRepresentation(nextBtImg);
    
    
    NSString *filePathForNextImg = [agendaScreenImgFolderPath stringByAppendingPathComponent:nextBtnImageName];       //set save as image name.
    
    [nextBtnImgData writeToFile:filePathForNextImg atomically:YES]; //Write the file

//Add image store
    NSString *addImgUrl= [NSString stringWithFormat:@"http://sitepark.ua/images/stories/plus_add_green.png"];
    
    UIImage *addImg = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:addImgUrl]]];
    NSData *addImgData = UIImagePNGRepresentation(addImg);
    
    
    NSString *filePathForAddImg = [agendaScreenImgFolderPath stringByAppendingPathComponent:addImgName];       //set save as image name.
    
    [addImgData writeToFile:filePathForAddImg atomically:YES]; //Write the file
    
//tickMrak image store
    NSString *tickMrakImgUrl= [NSString stringWithFormat:@"https://s3.amazonaws.com/recurbox.graphics/icons/checkmark.png"];
    
    UIImage *tickMrakImg = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:tickMrakImgUrl]]];
    NSData *tickMrakImgData = UIImagePNGRepresentation(tickMrakImg);
    
    
    NSString *filePathFortickMrakImg = [agendaScreenImgFolderPath stringByAppendingPathComponent:tickMrakImgName];       //set save as image name.
    
    [tickMrakImgData writeToFile:filePathFortickMrakImg atomically:YES]; //Write the file

    
}


#pragma mark - Method To get Menu Item Images Folder Path

NSString *getAgendaScreenImagesPath()
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *sponsorsFolder = [documentsPath stringByAppendingPathComponent:@"/AgendaScreenImages/"];
    
    return sponsorsFolder;
}


#pragma mark - Method To Load All Images in an File.

-(void)loadImagesToFiles
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *sposnsorsFolderPath = [documentsPath stringByAppendingPathComponent:@"/SpeakersImages/"];
    
    // NSString *sposnsorsFolderPath = getSpeakerImagesFilePath(nil);
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:sposnsorsFolderPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:sposnsorsFolderPath withIntermediateDirectories:NO attributes:nil error:Nil]; //Create folder
    
    for (int i=0; i < [_listOfSpeakers count]; i++)
    {
        NSString *imageUrlString= [NSString stringWithFormat:@"http://listoftablets.com/tiecon/uploads/%@",[[_listOfSpeakers objectAtIndex:i] valueForKey:@"IMAGE"]];
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrlString]]];
        NSData *pngData = UIImagePNGRepresentation(image);
        
        NSString *filePath = [sposnsorsFolderPath stringByAppendingPathComponent:[[_listOfSpeakers objectAtIndex:i] valueForKey:@"IMAGE"]]; //Add the file name
        [pngData writeToFile:filePath atomically:YES]; //Write the file
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backButtonClickAction:(id)sender
{
    pageIndex--;
    [self showDateAndDay];
    [_agendaTableView reloadData];
}

- (IBAction)nextButtonClickAction:(id)sender
{
    pageIndex++;
    [self showDateAndDay];
    _btnAgendaBackButton.enabled=YES;
    [_agendaTableView reloadData];
}



#pragma mark - Search Bar Delegate methods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText    // Method to search matching entry in Table View
{
    NSArray *arrayForSearch;     // array to Maintain initial entries in reload array
    NSString *temp = [NSString stringWithFormat:@"%@*",searchText];
    
    NSLog(@"%@",arrayForSearch);
    
    agendaRecordReloadArray =[self getSortedArray:Date];
    
    agendaRecordReloadArray = [agendaRecordReloadArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(TIME LIKE[cd] %@)", temp]];
    
    NSLog(@"%@",agendaRecordReloadArray);
    
    if ([temp isEqualToString:@""])
    {
        [searchBar resignFirstResponder];
    }
    
    [_agendaTableView reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    return YES;
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}


@end
