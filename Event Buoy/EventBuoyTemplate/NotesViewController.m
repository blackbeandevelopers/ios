
//
//  NotesViewController.m
//  Nasscom_Application 1.0
//
//  Created by Black Bean Engagement on 17/02/14.
//  Copyright (c) 2014 Black Bean Engagement. All rights reserved.
//

#import "NotesViewController.h"
#import "EventDetailViewController.h"
#import <sqlite3.h>
#import "ViewController.h"
#import "Event_Menu_ViewController.h"

@interface NotesViewController ()

@property (nonatomic,retain) IBOutlet UITextView *noteTextView;

- (NSString *)getnoteForEvent:(int)selectedID; // Method to get Written Note for Selected Event.

@property (weak, nonatomic) IBOutlet UILabel *notesTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *showMenuButton;

@end

NSString *notesTitleTextColor;


extern int selectedEventID;
extern NSString *getFilePath();    // Method To get file Path of Database
extern NSMutableDictionary *noteInfoDict;  // Dictionary Containing Note of Event
extern NSString *getMenuItemImagesPath();
NSString *menuButtonImageName;

@implementation NotesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"%d",selectedEventID);
    
    _noteTextView.text = [noteInfoDict objectForKey:@"NOTE"];
    notesTitleTextColor=@"2981B3";
    menuButtonImageName=@"eventlogo.png";
    
    _notesTitleLabel.textColor = [HexColorConvert colorWithHexString:notesTitleTextColor];
    _noteTextView.textColor = [HexColorConvert colorWithHexString:notesTitleTextColor];
    
    
    NSString *evenMenuScreenImagesPath = getMenuItemImagesPath(nil);
    
    NSString* filePathForEvetLogoButton = [evenMenuScreenImagesPath stringByAppendingPathComponent:[NSString stringWithString:menuButtonImageName]];
    NSData* eventLogoImageData = [NSData dataWithContentsOfFile:filePathForEvetLogoButton];
    
    [_showMenuButton setBackgroundImage:[UIImage imageWithData:eventLogoImageData] forState:UIControlStateNormal];
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    NSLog(@"%@",textView.description);
    NSLog(@"%@",textView.text);
    
    NSString *databasePath=getFilePath();
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    sqlite3 *database;
    sqlite3_stmt *statement;
    
    if ([filemgr fileExistsAtPath: databasePath])
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            NSString *insertSQL=[[NSString alloc]init];
            
            insertSQL = [NSString stringWithFormat: @"REPLACE INTO NOTE (ID,NOTE) VALUES ('%@','%@')",[NSNumber numberWithInt:selectedEventID],textView.text];  //
            
            const char *insert_stmt = [insertSQL UTF8String];
            
            sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL);
            
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                
            }
            else
                NSLog(@"error");
            
            sqlite3_finalize(statement);
            
            sqlite3_close(database);
        }
    }
    
    NSLog(@"%@",noteInfoDict);
    
    NSString *text= textView.text;
    
    [noteInfoDict setValue:text forKey:@"NOTE"];
    
}

#pragma mark -Method to get Written Note for Selected Event.

- (NSString *)getnoteForEvent:(int)selectedID
{
    NSString *databasePath=getFilePath();
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    NSString *Note=@"";
    
    sqlite3 *database;
    
    if ([filemgr fileExistsAtPath: databasePath])
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            NSString *sqlStatement = [NSString stringWithFormat:@"SELECT NOTE FROM NOTE WHERE ID ='%d'",selectedID];
            
            sqlite3_stmt *statement;
            
            if(sqlite3_prepare_v2(database, [sqlStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
            {
                while(sqlite3_step(statement) == SQLITE_ROW)
                {
                    NSInteger ID=sqlite3_column_int(statement, 0);
                    NSLog(@"%d",ID);
                    Note=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,1)];
                    NSLog(@"%@",Note);
                }
            }
            
            sqlite3_finalize(statement);
            
            sqlite3_close(database);
        }
    }
    return Note;

}

@end
