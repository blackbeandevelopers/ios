//
//  InfoCustomCell.m
//  Nasscom_Application 1.0
//
//  Created by Black Bean Engagement on 14/02/14.
//  Copyright (c) 2014 Black Bean Engagement. All rights reserved.
//

#import "InfoCustomCell.h"
#import "EventDetailViewController.h"
#import "SpeakersCustomCell.h"
#import <sqlite3.h>
#import "SpeakersViewController.h"   // Importing to get access method to get image file path


@implementation InfoCustomCell

extern NSArray *numberofSpeakersArray; // Contain All ID of Speakers.
NSArray *speakerArray;   // Array Contain All Information about Speaker of an Event
extern NSString *getFilePath();    // Method To get file Path of Database
extern NSString *getSpeakerImagesFilePath();  // Method to get Images File Path

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    _speakersTableView.delegate=self;
    _speakersTableView.dataSource=self;
    [super setSelected:selected animated:animated];
    speakerArray = [self getSpeakerInfo:numberofSpeakersArray];
    NSLog(@"%@",speakerArray);
    // Configure the view for the selected state
}

#pragma mark - Table View Delegate Methods

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber *IDnumber = [[speakerArray objectAtIndex:indexPath.row] valueForKey:@"ID"];
    NSLog(@"%@",IDnumber);
    [self writeToFile:IDnumber];
}

#pragma mark - Table View Datasource Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%@",numberofSpeakersArray);
    return [numberofSpeakersArray count];
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString *cellIdentifier =@"SpeakersCustomCell";
        
        SpeakersCustomCell *cell = (SpeakersCustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == Nil)
        {
            NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SpeakersCustomCell" owner:self options:nil];
            cell = [nibArray objectAtIndex:0];
        }
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        NSLog(@"%@",speakerArray);

        cell.speakerName.text = [[speakerArray objectAtIndex:indexPath.row] valueForKey:@"NAME"];
        cell.speakerDesignation.text = [[speakerArray objectAtIndex:indexPath.row] valueForKey:@"DETAILS1"];
    
        NSString *sponsorsFilePath = getSpeakerImagesFilePath();   // getting path of speaker image
        NSString *filePath = [sponsorsFilePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[[speakerArray objectAtIndex:indexPath.row] valueForKey:@"IMAGE"]]];
        NSData *pngData = [NSData dataWithContentsOfFile:filePath];
        cell.speakerImage.image = [UIImage imageWithData:pngData];
        [cell setNeedsDisplay];
 
    
//    dispatch_async(kBgQueue, ^{
//        
//        cell.speakerName.text = [[speakerArray objectAtIndex:indexPath.row] valueForKey:@"NAME"];
//        cell.speakerDesignation.text = [[speakerArray objectAtIndex:indexPath.row] valueForKey:@"DETAILS1"];
//        
//        NSString *sponsorsFilePath = getSpeakerImagesFilePath();   // getting path of speaker image
//        NSString *filePath = [sponsorsFilePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[[speakerArray objectAtIndex:indexPath.row] valueForKey:@"IMAGE"]]];
//        NSData *pngData = [NSData dataWithContentsOfFile:filePath];
//        
//
//        dispatch_async(dispatch_get_main_queue(), ^(void)
//                       {
//                           cell.speakerImage.image = [UIImage imageWithData:pngData];
//                           [cell.speakerImage setNeedsDisplay];
//                       });
//    });
    
        return cell;
}

#pragma mark - Text Field Delegate Methods

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{


}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
   // [textView resignFirstResponder];
    
    NSString * str = textView.text;
    
    NSLog(@"%@",str);
    
    return YES;
}

- (IBAction)doneEditing:(id)sender
{
    [_noteView resignFirstResponder];
}


#pragma mark - Method To get Info From Database by ID

- (NSArray *)getSpeakerInfo : (NSArray *)SpeakerID  // 1. Get ID in Int  2. Search in Database for Speaker ID
{
    NSMutableArray *speakerArray = [[NSMutableArray alloc]init];
    
    for (int i=0; i< [SpeakerID count ]; i++)
    {
        NSNumber *IDnum = [SpeakerID objectAtIndex:i];
        
        int ID = [IDnum intValue];
        
        NSString *databasePath=getFilePath();
        
        NSFileManager *filemgr = [NSFileManager defaultManager];
        
        sqlite3 *database;
        
        if ([filemgr fileExistsAtPath: databasePath])
        {
            const char *dbpath = [databasePath UTF8String];
            
            if (sqlite3_open(dbpath, &database) == SQLITE_OK)
            {
                //const char *sqlStatement = "SELECT * FROM EVENT ";
                
                NSString *sqlStatement = [NSString stringWithFormat:@"SELECT * FROM SPEAKERS WHERE ID = %d",ID];
                
                sqlite3_stmt *statement;
                
                if(sqlite3_prepare_v2(database, [sqlStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
                {
                    while(sqlite3_step(statement) == SQLITE_ROW)
                    {
                        NSInteger ID=sqlite3_column_int(statement, 0);
                        NSString *NAME=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,1)];
                        NSString *DETAILS1=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,2)];
                        NSString *DETAILS2=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,3)];
                        NSString *IMAGE=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,4)];
                        NSString *INFO=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,5)];
                        NSString *SESSIONS=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,6)];
                        NSString *SOCIAL=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(statement,7)];
                        
                        
                        NSDictionary *recordDictionary=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:ID],@"ID",NAME,@"NAME",DETAILS1,@"DETAILS1",DETAILS2,@"DETAILS2",IMAGE,@"IMAGE",INFO,@"INFO",SESSIONS,@"SESSIONS",SOCIAL,@"SOCIAL",Nil];      // getting record on dictionary
                        
                        [speakerArray addObject:recordDictionary];   // Adding Each Record in an Array
                    }
                }
                sqlite3_finalize(statement);
                sqlite3_close(database);
            }
        }
    }


    return speakerArray;
}


- (IBAction)onNoteButtonClick:(id)sender
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"noteView"];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    //[InfoCustomCell performSegueWithIdentifier:@"notesView" sender:self];    // Move to Event Detail Page
}

#pragma mark - Write Slelected Speaker ID to .plist file

- (void)writeToFile :(NSNumber*)selectedID
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"Tiecon.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: path])
    {
        path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat: @"Tiecon.plist"] ];
    }
    
    NSMutableDictionary *data;
    
    if ([fileManager fileExistsAtPath: path])
    {
        data = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    }
    else
    {
        // If the file doesn’t exist, create an empty dictionary
        data = [[NSMutableDictionary alloc] init];
    }
    
    //To insert the data into the plist
    [data setObject:selectedID forKey:@"SelectedID"];
    [data writeToFile: path atomically:YES];

}
@end
